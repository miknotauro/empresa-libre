
var toolbar_nomina = [
    {view: 'button', id: 'cmd_empleados', label: 'Empleados', type: 'iconButton',
        autowidth: true, icon: 'users'},
    {},
    {view: 'button', id: 'cmd_nomina_report', label: 'Reporte', type: 'iconButton',
        autowidth: true, icon: 'table'},
    {},
    {view: 'button', id: 'cmd_nomina_delete', label: 'Eliminar',
        type: 'iconButton', autowidth: true, icon: 'minus'},
]


var toolbar_nomina_util = [
    {view: 'button', id: 'cmd_nomina_import', label: 'Importar',
        type: 'iconButton', autowidth: true, icon: 'upload'},
    {view: 'button', id: 'cmd_nomina_timbrar', label: 'Timbrar',
        type: 'iconButton', autowidth: true, icon: 'ticket'},
    {view: 'button', id: 'cmd_nomina_sat', label: 'SAT',
        type: 'iconButton', autowidth: true, icon: 'check-circle'},
    {view: 'button', id: 'cmd_nomina_log', label: 'Log',
        type: 'iconButton', autowidth: true, icon: 'download'},
    {view: 'button', id: 'cmd_nomina_download', label: 'Descargar',
        type: 'iconButton', autowidth: true, icon: 'download'},
    {},
    {view: 'button', id: 'cmd_nomina_cancel', label: 'Cancelar',
        type: 'iconButton', autowidth: true, icon: 'ban'},
]


var toolbar_nomina_filter = [
    {view: 'richselect', id: 'filter_year_nomina', label: 'Año',
        labelAlign: 'right', labelWidth: 50, width: 150, options: []},
    {view: 'richselect', id: 'filter_month_nomina', label: 'Mes',
        labelAlign: 'right', labelWidth: 50, width: 200, options: months},
    {view: 'daterangepicker', id: 'filter_dates_nomina', label: 'Fechas',
        labelAlign: 'right', width: 300},
    {},
    {view: 'button', id: 'cmd_nomina_without_stamp', label: 'Sin Timbrar',
        type: 'iconButton', autowidth: true, icon: 'filter'},
]


var grid_cols_nomina = [
    {id: 'index', header: '#', adjust: 'data', css: 'right',
        footer: {content: 'countRows', colspan: 3, css: 'right'}},
    {id: "id", header:"ID", hidden:true},
    {id: "serie", header: ["Serie"], adjust: "header"},
    {id: 'folio', header: ['Folio', {content: 'numberFilter'}], adjust: 'header',
        sort: 'int', css: 'right', footer: {text: 'Recibos', colspan: 3}},
    {id: "fecha", header: ["Fecha y Hora"], adjust: "data", sort: "string"},
    {id: "estatus", header: ["Estatus", {content: "selectFilter"}],
        adjust: "data", sort:"string"},
    {id: 'fecha_pago', header: ['Fecha de Pago', {content: 'selectFilter'}],
        adjust: 'data', sort: 'string'},
    {id: 'total', header: ['Total', {content: 'numberFilter'}], width: 150,
        sort: 'int', format: webix.i18n.priceFormat, css: 'right',
        footer: {content: 'summActive', css: 'right'}},
    {id: "empleado", header: ["Empleado", {content: "selectFilter"}],
        fillspace:true, sort:"string"},
    {id: 'xml', header: 'XML', adjust: 'data', template: get_icon('xml')},
    {id: 'pdf', header: 'PDF', adjust: 'data', template: get_icon('pdf')},
]


var grid_nomina = {
    view: 'datatable',
    id: 'grid_nomina',
    select: 'row',
    multiselect: true,
    scrollY: true,
    adjust: true,
    footer: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_cols_nomina,
    scheme:{
        $change:function(item){
            if (item.estatus == 'Cancelado'){
                item.$css = 'cancel'
            }
        }
    },
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var rows_nomina_home = [
    {view: 'toolbar', elements: toolbar_nomina},
    {view: 'toolbar', elements: toolbar_nomina_util},
    {view: 'toolbar', elements: toolbar_nomina_filter},
    grid_nomina,
]


var toolbar_nomina_empleados = [
    {view: 'button', id: 'cmd_new_empleado', label: 'Nuevo', type: 'iconButton',
        autowidth: true, icon: 'user-plus'},
    {view: 'button', id: 'cmd_edit_empleado', label: 'Editar', type: 'iconButton',
        autowidth: true, icon: 'user'},
    {view: 'button', id: 'cmd_delete_empleado', label: 'Eliminar', type: 'iconButton',
        autowidth: true, icon: 'user-times'},
    {},
    {view: 'button', id: 'cmd_import_empleados', label: 'Importar',
        type: 'iconButton', autowidth: true, icon: 'upload'},
    {},
    {view: 'button', id: 'cmd_close_empleados', label: 'Cerrar', type: 'iconButton',
        autowidth: true, icon: 'times-circle-o'},
]


var grid_cols_empleados = [
    {id: 'index', header: '#', adjust: 'data', css: 'right',
        footer: {content: 'countRows', colspan: 3, css: 'right'}},
    {id: "id", header:"ID", hidden:true},
    {id: "num_empleado", header: ["No Empleado"], adjust: "data"},
    {id: "rfc", header: ["RFC", {content: 'textFilter'}], adjust: "data", sort: "string"},
    {id: "curp", header: ["CURP"], adjust: "data", hidden:true, sort: "string"},
    {id: "nombre_completo", header: ["Empleado", {content: 'textFilter'}],
        adjust: "data", fillspace: true, sort: "string"},
    {id: 'imss', header: ['IMSS'], adjust: 'data'},
    {id: 'salario_base', header: ['Salario Base'], adjust: 'header',
        format: webix.i18n.priceFormat, css: 'right'},
    {id: 'salario_diario', header: ['Salario Diario'], adjust: 'header',
        format: webix.i18n.priceFormat, css: 'right'},
    {id: "fecha_ingreso", header: ["Fecha de Ingreso"], adjust: "header",
        sort: "string"},
]


var grid_empleados = {
    view: 'datatable',
    id: 'grid_employees',
    select: 'row',
    scrollY: true,
    adjust: true,
    footer: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_cols_empleados,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var rows_nomina_empleados = [
    {view: 'toolbar', elements: toolbar_nomina_empleados},
    grid_empleados,
]


var multi_nomina = {
    id: 'multi_nomina',
    view: 'multiview',
    animate: true,
    cells:[
        {id: 'nomina_home', rows: rows_nomina_home},
        {id: 'nomina_empleados', rows: rows_nomina_empleados},
    ],
}


var app_nomina = {
    id: 'app_nomina',
    rows:[
        {view: 'template', type: 'header', template: 'Timbrado de Nómina'},
        multi_nomina
    ],
}


var body_import_employees = {rows: [
    {view: 'form', id: 'form_upload_employees', rows: [
        {cols: [{},
            {view: 'uploader', id: 'up_employees', autosend: false,
                link: 'lst_upload_employees', value: 'Seleccionar Plantilla',
                upload: '/files/employees'}, {}]},
        {cols: [
            {view: 'list',  id: 'lst_upload_employees', name: 'lst_employees',
                type: 'uploader', autoheight: true, borderless: true}]},
        {cols: [{}, {view: 'button', id: 'cmd_import_employees',
            label: 'Importar Empleados'}, {}]},
    ]},
],}


var win_import_employees = {
    init: function(){
        webix.ui({
            view: 'window',
            id: 'win_import_employees',
            width: 400,
            modal: true,
            position: 'center',
            head: 'Importar Empleados',
            body: body_import_employees,
        })
        $$('cmd_import_employees').attachEvent('onItemClick', cmd_import_employees_click)
        $$('up_employees').attachEvent('onUploadComplete', up_employees_upload_complete)
    }
}


var body_import_nomina = {rows: [
    {view: 'form', id: 'form_upload_nomina', rows: [
        {cols: [{},
            {view: 'uploader', id: 'up_nomina', autosend: false,
                link: 'lst_upload_nomina', value: 'Seleccionar Plantilla',
                upload: '/files/nomina'}, {}]},
        {cols: [
            {view: 'list',  id: 'lst_upload_nomina', name: 'lst_nomina',
                type: 'uploader', autoheight: true, borderless: true}]},
        {cols: [{}, {view: 'button', id: 'cmd_import_template_nomina',
            label: 'Importar Nómina'}, {}]},
    ]},
],}


var win_import_nomina = {
    init: function(){
        webix.ui({
            view: 'window',
            id: 'win_import_nomina',
            width: 400,
            modal: true,
            position: 'center',
            head: 'Importar Nómina',
            body: body_import_nomina,
        })
        $$('cmd_import_template_nomina').attachEvent('onItemClick', cmd_import_template_nomina_click)
        $$('up_nomina').attachEvent('onUploadComplete', up_nomina_upload_complete)
    }
}
