#!/usr/bin/env python3

# ~ Empresa Libre
# ~ Copyright (C) 2016-2018  Mauricio Baeza Servin (web@correolibre.net)
# ~
# ~ This program is free software: you can redistribute it and/or modify
# ~ it under the terms of the GNU General Public License as published by
# ~ the Free Software Foundation, either version 3 of the License, or
# ~ (at your option) any later version.
# ~
# ~ This program is distributed in the hope that it will be useful,
# ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
# ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# ~ GNU General Public License for more details.
# ~
# ~ You should have received a copy of the GNU General Public License
# ~ along with this program.  If not, see <http://www.gnu.org/licenses/>.

import base64
import datetime
import getpass
import hashlib
import io
import json
import locale
import mimetypes
import os
import re
import requests
import sqlite3
import socket
import subprocess
import tempfile
import textwrap
import threading
import time
import unicodedata
import uuid
import zipfile

from io import BytesIO
from math import trunc
from pathlib import Path
from xml.etree import ElementTree as ET
from xml.dom.minidom import parseString

# ~ import pdfkit
# ~ from weasyprint import HTML, CSS
# ~ from weasyprint.fonts import FontConfiguration

try:
    import uno
    from com.sun.star.beans import PropertyValue
    from com.sun.star.awt import Size
    from com.sun.star.view.PaperFormat import LETTER
    APP_LIBO = True
except ImportError:
    APP_LIBO = False

import pyqrcode
from dateutil import parser
from lxml import etree

import mako.runtime
from mako.exceptions import TopLevelLookupException
mako.runtime.UNDEFINED = ''

from .helper import CaseInsensitiveDict, NumLet, SendMail, TemplateInvoice, \
    SeaFileAPI, PrintTicket
from settings import DEBUG, MV, log, template_lookup, COMPANIES, DB_SAT, \
    PATH_XSLT, PATH_XSLTPROC, PATH_OPENSSL, PATH_TEMPLATES, PATH_MEDIA, PRE, \
    PATH_XMLSEC, TEMPLATE_CANCEL, DEFAULT_SAT_PRODUCTO, DECIMALES, DIR_FACTURAS

from settings import SEAFILE_SERVER, USAR_TOKEN, API, DECIMALES_TAX
from .configpac import AUTH


# ~ v2
from settings import (
    EXT,
    MXN,
    PATHS,
)


def _call(args):
    return subprocess.check_output(args, shell=True).decode()


def _get_md5(data):
    return hashlib.md5(data.encode()).hexdigest()


def save_temp(data, modo='wb'):
    path = tempfile.mkstemp()[1]
    with open(path, modo) as f:
        f.write(data)
    return path


def save_file(path, data, modo='wb'):
    try:
        with open(path, modo) as f:
            f.write(data)
        return True
    except:
        return False


def _join(*paths):
    return os.path.join(*paths)


def _kill(path):
    try:
        os.remove(path)
    except:
        pass
    return


def get_pass():
    password = getpass.getpass('Introduce la contraseña: ')
    pass2 = getpass.getpass('Confirma la contraseña: ')

    if password != pass2:
        msg = 'Las contraseñas son diferentes'
        return False, msg
    password = password.strip()
    if not password:
        msg = 'La contraseña es necesaria'
        return False, msg

    return True, password


def get_value(arg):
    value = input('Introduce el {}: '.format(arg)).strip()
    if not value:
        msg = 'El {} es requerido'.format(arg)
        log.error(msg)
        return ''
    return value


def _valid_db_companies():
    con = sqlite3.connect(COMPANIES)
    sql = """
        CREATE TABLE IF NOT EXISTS names(
            rfc TEXT NOT NULL COLLATE NOCASE UNIQUE,
            con TEXT NOT NULL
        );
    """
    cursor = con.cursor()
    cursor.executescript(sql)
    cursor.close()
    con.close()
    return


def _get_args(rfc):
    _valid_db_companies()
    con = sqlite3.connect(COMPANIES)
    cursor = con.cursor()
    sql = "SELECT con FROM names WHERE rfc=?"
    cursor.execute(sql, (rfc,))
    values = cursor.fetchone()
    if values is None:
        msg = 'No se encontró el RFC'
        log.error(msg)
        return ''

    cursor.close()
    con.close()
    return values[0]


def get_rfcs():
    _valid_db_companies()
    con = sqlite3.connect(COMPANIES)
    cursor = con.cursor()
    sql = "SELECT * FROM names"
    cursor.execute(sql)
    values = cursor.fetchall()
    cursor.close()
    con.close()
    return values


def get_con(rfc=''):
    if not rfc:
        rfc = get_value('RFC').upper()
        if not rfc:
            return {}

    args = _get_args(rfc.upper())
    if not args:
        return {}
    return loads(args)


def get_sat_key(table, key):
    con = sqlite3.connect(DB_SAT)
    cursor = con.cursor()
    sql = 'SELECT key, name FROM {} WHERE key=?'.format(table)
    cursor.execute(sql, (key,))
    data = cursor.fetchone()
    cursor.close()
    con.close()
    if data is None:
        return {'ok': False, 'text': 'No se encontró la clave'}
    return {'ok': True, 'text': data[1]}


def get_sat_monedas(key):
    con = sqlite3.connect(DB_SAT)
    con.row_factory = sqlite3.Row
    cursor = con.cursor()

    filtro = '%{}%'.format(key)
    sql = "SELECT * FROM monedas WHERE key LIKE ? OR name LIKE ?"

    cursor.execute(sql, [filtro, filtro])
    data = cursor.fetchall()
    cursor.close()
    con.close()
    if data is None:
        return ()

    data = [dict(r) for r in data]
    return tuple(data)


def get_sat_unidades(key):
    con = sqlite3.connect(DB_SAT)
    con.row_factory = sqlite3.Row
    cursor = con.cursor()

    filtro = '%{}%'.format(key)
    sql = "SELECT * FROM unidades WHERE key LIKE ? OR name LIKE ?"

    cursor.execute(sql, [filtro, filtro])
    data = cursor.fetchall()
    cursor.close()
    con.close()
    if data is None:
        return ()

    data = [dict(r) for r in data]
    return tuple(data)


def get_sat_productos(key):
    con = sqlite3.connect(DB_SAT)
    con.row_factory = sqlite3.Row
    cursor = con.cursor()

    filtro = '%{}%'.format(key)
    sql = "SELECT * FROM productos WHERE key LIKE ? OR name LIKE ?"

    cursor.execute(sql, [filtro, filtro])
    data = cursor.fetchall()
    cursor.close()
    con.close()
    if data is None:
        return ()

    data = [dict(r) for r in data]
    return tuple(data)


def now():
    return datetime.datetime.now().replace(microsecond=0)


def today():
    return datetime.date.today()


def get_token():
    return _get_hash(uuid.uuid4().hex)


def get_mimetype(path):
    mt = mimetypes.guess_type(path)[0]
    return mt or 'application/octet-stream'


def is_file(path):
    return os.path.isfile(path)


def get_stream(path):
    return get_file(path), get_size(path)


def get_file(path):
    return open(path, 'rb')


def get_files(path, ext='xml'):
    docs = []
    for folder, _, files in os.walk(path):
        pattern = re.compile('\.{}'.format(ext), re.IGNORECASE)
        docs += [os.path.join(folder,f) for f in files if pattern.search(f)]
    return tuple(docs)


def read_file(path, mode='rb'):
    return open(path, mode).read()


def get_size(path):
    return os.path.getsize(path)


def get_template(name, data={}):
    # ~ print ('NAME', name)
    template = template_lookup.get_template(name)
    return template.render(**data)


def get_custom_styles(name, default='plantilla_factura.json'):
    path = _join(PATH_MEDIA, 'templates', name.lower())
    if is_file(path):
        with open(path) as fh:
            return loads(fh.read())

    path = _join(PATH_TEMPLATES, default)
    if is_file(path):
        with open(path) as fh:
            return loads(fh.read())

    return {}


def get_template_ods(name, default='plantilla_factura.ods'):
    path = _join(PATH_MEDIA, 'templates', name.lower())
    if is_file(path):
        return path

    if 'pagos' in name:
        default='plantilla_pagos.ods'

    path = _join(PATH_TEMPLATES, default)
    if is_file(path):
        return path

    return ''


def dumps(data):
    return json.dumps(data, default=str)


def loads(data):
    return json.loads(data)


def import_json(path):
    return loads(read_file(path, 'r'))


def clean(values):
    for k, v in values.items():
        if isinstance(v, str):
            values[k] = v.strip()
    return values


def parse_con(values):
    data = values.split('|')
    try:
        con = {'type': data[0]}
        if con['type'] == 'sqlite':
            con['name'] = data[1]
        else:
            if data[1]:
                con['host'] = data[1]
            if data[2]:
                con['port'] = data[2]
            con['name'] = data[3]
            con['user'] = data[4]
            con['password'] = data[5]
        return con
    except IndexError:
        return {}


def spaces(value):
    return '\n'.join([' '.join(l.split()) for l in value.split('\n')])


def to_slug(string):
    value = (unicodedata.normalize('NFKD', string)
        .encode('ascii', 'ignore')
        .decode('ascii').lower())
    return value.replace(' ', '_')


class Certificado(object):

    def __init__(self, paths):
        self._path_key = paths['path_key']
        self._path_cer = paths['path_cer']
        self._modulus = ''
        self.error = ''

    def _kill(self, path):
        try:
            os.remove(path)
        except:
            pass
        return

    def _get_info_cer(self, session_rfc):
        data = {}
        args = 'openssl x509 -inform DER -in {}'
        try:
            cer_pem = _call(args.format(self._path_cer))
        except Exception as e:
            self.error = 'No se pudo convertir el CER en PEM'
            return data

        args = 'openssl enc -base64 -in {}'
        try:
            cer_txt = _call(args.format(self._path_cer))
        except Exception as e:
            self.error = 'No se pudo convertir el CER en TXT'
            return data

        args = 'openssl x509 -inform DER -in {} -noout -{}'
        try:
            result = _call(args.format(self._path_cer, 'purpose')).split('\n')[3]
        except Exception as e:
            self.error = 'No se puede saber si es FIEL'
            return data

        if result == 'SSL server : No':
            self.error = 'El certificado es FIEL'
            return data

        result = _call(args.format(self._path_cer, 'serial'))
        serie = result.split('=')[1].split('\n')[0][1::2]
        result = _call(args.format(self._path_cer, 'subject'))
        #~ Verificar si es por la version de OpenSSL
        t1 = 'x500UniqueIdentifier = '
        t2 = 'x500UniqueIdentifier='
        if t1 in result:
            rfc = result.split(t1)[1][:13].strip()
        elif t2 in result:
            rfc = result.split(t2)[1][:13].strip()
        else:
            self.error = 'No se pudo obtener el RFC del certificado'
            print ('\n', result)
            return data

        if not DEBUG:
            if not rfc == session_rfc:
                self.error = 'El RFC del certificado no corresponde.'
                return data

        dates = _call(args.format(self._path_cer, 'dates')).split('\n')
        desde  = parser.parse(dates[0].split('=')[1])
        hasta  = parser.parse(dates[1].split('=')[1])
        self._modulus = _call(args.format(self._path_cer, 'modulus'))

        data['cer'] = read_file(self._path_cer)
        data['cer_pem'] = cer_pem
        data['cer_txt'] = cer_txt.replace('\n', '')
        data['serie'] = serie
        data['rfc'] = rfc
        data['desde'] = desde.replace(tzinfo=None)
        data['hasta'] = hasta.replace(tzinfo=None)
        return data

    def _get_p12(self, password, rfc, token):
        tmp_cer = tempfile.mkstemp()[1]
        tmp_key = tempfile.mkstemp()[1]
        tmp_p12 = tempfile.mkstemp()[1]

        args = 'openssl x509 -inform DER -in "{}" -out "{}"'
        _call(args.format(self._path_cer, tmp_cer))
        args = 'openssl pkcs8 -inform DER -in "{}" -passin pass:"{}" -out "{}"'
        _call(args.format(self._path_key, password, tmp_key))

        args = 'openssl pkcs12 -export -in "{}" -inkey "{}" -name "{}" ' \
            '-passout pass:"{}" -out "{}"'
        _call(args.format(tmp_cer, tmp_key, rfc, token, tmp_p12))
        data = read_file(tmp_p12)

        self._kill(tmp_cer)
        self._kill(tmp_key)
        self._kill(tmp_p12)

        return data

    def _get_info_key(self, password, rfc, token):
        data = {}

        args = 'openssl pkcs8 -inform DER -in "{}" -passin pass:"{}"'
        try:
            result = _call(args.format(self._path_key, password))
        except Exception as e:
            self.error = 'Contraseña incorrecta'
            return data

        args = 'openssl pkcs8 -inform DER -in "{}" -passin pass:"{}" | ' \
            'openssl rsa -noout -modulus'
        mod_key = _call(args.format(self._path_key, password))

        if self._modulus != mod_key:
            self.error = 'Los archivos no son pareja'
            return data

        args = "openssl pkcs8 -inform DER -in '{}' -passin pass:'{}' | " \
            "openssl rsa -des3 -passout pass:'{}'".format(
            self._path_key, password, token)
        key_enc = _call(args)

        data['key'] = read_file(self._path_key)
        data['key_enc'] = key_enc
        data['p12'] = self._get_p12(password, rfc, token)
        return data

    def validate(self, password, rfc, auth):
        token =  _get_md5(rfc)
        if USAR_TOKEN:
            token = auth['PASS']
            if AUTH['DEBUG']:
                token = AUTH['PASS']

        if not self._path_key or not self._path_cer:
            self.error = 'Error en las rutas temporales del certificado'
            return {}

        data = self._get_info_cer(rfc)
        if not data:
            return {}

        llave = self._get_info_key(password, rfc, token)
        if not llave:
            return {}

        data.update(llave)

        self._kill(self._path_key)
        self._kill(self._path_cer)
        return data


def make_xml(data, certificado, auth):
    from .cfdi_xml import CFDI

    token =  _get_md5(certificado.rfc)
    if USAR_TOKEN:
        token = auth['PASS']
        if AUTH['DEBUG']:
            token = AUTH['PASS']

    if DEBUG:
        data['emisor']['Rfc'] = certificado.rfc
        data['emisor']['RegimenFiscal'] = '603'

    cfdi = CFDI()
    xml = cfdi.get_xml(data)

    data = {
        'xsltproc': PATH_XSLTPROC,
        'xslt': _join(PATH_XSLT, 'cadena.xslt'),
        'xml': save_temp(xml, 'w'),
        'openssl': PATH_OPENSSL,
        'key': save_temp(certificado.key_enc, 'w'),
        'pass': token,
    }
    args = '"{xsltproc}" "{xslt}" "{xml}" | ' \
        '"{openssl}" dgst -sha256 -sign "{key}" -passin pass:"{pass}" | ' \
        '"{openssl}" enc -base64 -A'.format(**data)
    sello = _call(args)

    _kill(data['xml'])
    _kill(data['key'])

    return cfdi.add_sello(sello)


def timbra_xml(xml, auth):
    from .pac import Finkok as PAC

    if not DEBUG and not auth:
        msg = 'Sin datos para timbrar'
        result = {'ok': True, 'error': msg}
        return result

    result = {'ok': True, 'error': ''}
    pac = PAC(auth)
    new_xml = pac.timbra_xml(xml)
    if not new_xml:
        result['ok'] = False
        result['error'] = pac.error
        if pac.error.startswith('413'):
            return _ecodex_timbra_xml(xml)
        else:
            return result

    result['xml'] = new_xml
    result['uuid'] = pac.uuid
    result['fecha'] = pac.fecha
    return result


def _get_uuid_fecha(xml):
    doc = parse_xml(xml)
    version = doc.attrib['Version']
    node = doc.find('{}Complemento/{}TimbreFiscalDigital'.format(
        PRE[version], PRE['TIMBRE']))
    return node.attrib['UUID'], node.attrib['FechaTimbrado']


def _ecodex_timbra_xml(xml):
    from .pac import Ecodex as PAC
    from .configpac import ecodex

    auth, url = ecodex(DEBUG)

    pac = PAC(auth, url)
    xml = pac.timbra_xml(xml)
    if xml:
        data = {'ok': True, 'error': ''}
        data['xml'] = xml
        uuid, fecha = _get_uuid_fecha(xml)
        data['uuid'] = uuid
        data['fecha'] = fecha
        return data

    msg = pac.error
    return {'ok': False, 'error': msg}


def get_sat(xml):
    from .pac import get_status_sat
    return get_status_sat(xml)


class LIBO(object):
    HOST = 'localhost'
    PORT = '8100'
    ARG = 'socket,host={},port={};urp;StarOffice.ComponentContext'.format(
        HOST, PORT)
    CMD = ['soffice',
        '-env:SingleAppInstance=false',
        '-env:UserInstallation=file:///tmp/LIBO_Process8100',
        '--headless', '--norestore', '--nologo', '--accept={}'.format(ARG)]
    CELL_STYLE = {
        'EUR': 'euro',
    }

    def __init__(self):
        self._app = None
        self._start_office()
        self._init_values()

    def _init_values(self):
        self._es_pre = False
        self._ctx = None
        self._sm = None
        self._desktop = None
        self._currency = MXN
        self._total_cantidades = 0
        if self.is_running:
            ctx = uno.getComponentContext()
            service = 'com.sun.star.bridge.UnoUrlResolver'
            resolver = ctx.ServiceManager.createInstanceWithContext(service, ctx)
            self._ctx = resolver.resolve('uno:{}'.format(self.ARG))
            self._sm = self._ctx.ServiceManager
            self._desktop = self._create_instance('com.sun.star.frame.Desktop')
        return

    def _create_instance(self, name, with_context=True):
        if with_context:
            instance = self._sm.createInstanceWithContext(name, self._ctx)
        else:
            instance = self._sm.createInstance(name)
        return instance

    @property
    def is_running(self):
        try:
            s = socket.create_connection((self.HOST, self.PORT), 5.0)
            s.close()
            return True
        except ConnectionRefusedError:
            return False

    def _start_office(self):
        if self.is_running:
            return

        for i in range(3):
            self.app = subprocess.Popen(self.CMD,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            time.sleep(5)
            if self.is_running:
                break
        return

    def _set_properties(self, properties):
        pl = []
        for k, v in properties.items():
            pv = PropertyValue()
            pv.Name = k
            pv.Value = v
            pl.append(pv)
        return tuple(pl)

    def _doc_open(self, path, options):
        options = self._set_properties(options)
        path = self._path_url(path)
        try:
            doc = self._desktop.loadComponentFromURL(path, '_blank', 0, options)
            return doc
        except:
            return None

    def _path_url(self, path):
        if path.startswith('file://'):
            return path
        return uno.systemPathToFileUrl(path)

    def close(self):
        if self.is_running:
            if not self._desktop is None:
                self._desktop.terminate()
            if not self._app is None:
                self._app.terminate()
        return

    def _read(self, path):
        try:
            return open(path, 'rb').read()
        except:
            return b''

    def _clean(self):
        self._sd.SearchRegularExpression = True
        self._sd.setSearchString("\{(\w.+)\}")
        self._search.replaceAll(self._sd)
        return

    def _cancelado(self, cancel):
        if not cancel:
            pd = self._sheet.getDrawPage()
            if pd.getCount():
                pd.remove(pd.getByIndex(0))
        return

    def _set_search(self):
        self._sheet = self._template.getSheets().getByIndex(0)
        try:
            self._search = self._sheet.getPrintAreas()[0]
        except IndexError:
            self._search = self._sheet.getRangeAddress()

        self._search = self._sheet.getCellRangeByPosition(
            self._search.StartColumn,
            self._search.StartRow,
            self._search.EndColumn,
            self._search.EndRow
        )
        self._sd = self._sheet.createSearchDescriptor()
        try:
            self._sd.SearchCaseSensitive = False
        except:
            print ('SD', self._sd)
        return

    def _next_cell(self, cell):
        col = cell.getCellAddress().Column
        row = cell.getCellAddress().Row + 1
        return self._sheet.getCellByPosition(col, row)

    def _copy_cell(self, cell):
        destino = self._next_cell(cell)
        self._sheet.copyRange(destino.getCellAddress(), cell.getRangeAddress())
        return destino

    def _set_cell(self, k='', v=None, cell=None, value=False):
        if k:
            self._sd.setSearchString(k)
            ranges = self._search.findAll(self._sd)
            if ranges:
                ranges = ranges.getRangeAddressesAsString().split(';')
                for r in ranges:
                    for c in r.split(','):
                        cell = self._sheet.getCellRangeByName(c)
                        if v is None:
                            return cell
                        if cell.getImplementationName() == 'ScCellObj':
                            pattern = re.compile(k, re.IGNORECASE)
                            nv = pattern.sub(v, cell.getString())
                            if value:
                                cell.setValue(nv)
                            else:
                                cell.setString(nv)
                return cell
        if cell:
            if cell.getImplementationName() == 'ScCellObj':
                ca = cell.getCellAddress()
                new_cell = self._sheet.getCellByPosition(ca.Column, ca.Row + 1)
                if value:
                    new_cell.setValue(v)
                else:
                    new_cell.setString(v)
                return new_cell

    def _comprobante(self, data):
        for k, v in data.items():
            if k.lower() in ('total', 'descuento', 'subtotal', 'totalgravado', 'totalexento'):
                self._set_cell('{cfdi.%s}' % k, v, value=True)
            else:
                self._set_cell('{cfdi.%s}' % k, v)
        return

    def _emisor(self, data):
        for k, v in data.items():
            self._set_cell('{emisor.%s}' % k, v)
        return

    def _receptor(self, data):
        for k, v in data.items():
            if k.lower() in ('salariobasecotapor', 'salariodiariointegrado'):
                self._set_cell('{receptor.%s}' % k, v, value=True)
            else:
                self._set_cell('{receptor.%s}' % k, v)
        return

    def _copy_row(self, cell):
        row = cell.getCellAddress().Row
        source = self._sheet.getRows().getByIndex(row)
        nc = self._next_cell(cell)
        self._sheet.copyRange(nc.getCellAddress(), source.getRangeAddress())
        return

    def _clean_rows(self, row, count):
        for i in range(count):
            source = self._sheet.getRows().getByIndex(row + i)
            source.clearContents(5)
        return

    def _copy_paste_rows(self, cell, count):
        dispatch = self._create_instance('com.sun.star.frame.DispatchHelper')

        row = cell.getCellAddress().Row
        source = self._sheet.getRows().getByIndex(row)
        self._template.getCurrentController().select(source)
        frame = self._template.getCurrentController().getFrame()
        dispatch.executeDispatch(frame, '.uno:Copy', '', 0, ())

        target = self._sheet.getCellRangeByPosition(0, row + 1, 0, row + count)
        self._template.getCurrentController().select(target)
        dispatch.executeDispatch(frame, '.uno:Paste', '', 0, ())
        return

    def _get_style(self, cell):
        if cell is None:
            return ''

        match = re.match(r"([a-z]+)([0-9]+)", cell.CellStyle, re.I)
        if not match:
            return ''

        currency = self.CELL_STYLE.get(self._currency, 'peso')
        return '{}{}'.format(currency, match.groups()[1])

    def _conceptos(self, data, pakings):
        first = True
        col1 = []
        col2 = []
        col3 = []
        col4 = []
        col5 = []
        col6 = []
        col7 = []
        col8 = []
        count = len(data) - 1
        for i, concepto in enumerate(data):
            key = concepto.get('noidentificacion', '')
            description = concepto['descripcion']
            unidad = concepto['unidad']
            cantidad = concepto['cantidad']
            valor_unitario = concepto['valorunitario']
            importe = concepto['importe']
            descuento = concepto.get('descuento', '0.0')
            if first:
                first = False
                cell_1 = self._set_cell('{noidentificacion}', key)
                cell_2 = self._set_cell('{descripcion}', description)
                cell_3 = self._set_cell('{unidad}', unidad)
                cell_4 = self._set_cell('{cantidad}', cantidad, value=True)
                cell_5 = self._set_cell('{valorunitario}', valor_unitario, value=True)
                cell_6 = self._set_cell('{importe}', importe, value=True)
                cell_7 = self._set_cell('{descuento}', descuento, value=True)
                if pakings:
                    cell_8 = self._set_cell('{empaque}', pakings[i], value=True)
                if len(data) > 1:
                    row = cell_1.getCellAddress().Row + 1
                    self._sheet.getRows().insertByIndex(row, count)
                    self._copy_paste_rows(cell_1, count)
                row = cell_1.getCellAddress().Row
            else:
                col1.append((key,))
                col2.append((description,))
                col3.append((unidad,))
                col4.append((float(cantidad),))
                col5.append((float(valor_unitario),))
                col6.append((float(importe),))
                col7.append((float(descuento),))
                if pakings:
                    col8.append((pakings[i],))
            self._total_cantidades += float(cantidad)
        if not count:
            return

        style_5 = self._get_style(cell_5)
        style_6 = self._get_style(cell_6)
        style_7 = self._get_style(cell_7)
        style_8 = ''
        if pakings:
            style_8 = self._get_style(cell_8)

        col = cell_1.getCellAddress().Column
        target1 = self._sheet.getCellRangeByPosition(col, row+1, col, row+count)
        col = cell_2.getCellAddress().Column
        target2 = self._sheet.getCellRangeByPosition(col, row+1, col, row+count)
        col = cell_3.getCellAddress().Column
        target3 = self._sheet.getCellRangeByPosition(col, row+1, col, row+count)
        col = cell_4.getCellAddress().Column
        target4 = self._sheet.getCellRangeByPosition(col, row+1, col, row+count)
        col = cell_5.getCellAddress().Column
        target5 = self._sheet.getCellRangeByPosition(col, row+1, col, row+count)
        col = cell_6.getCellAddress().Column
        target6 = self._sheet.getCellRangeByPosition(col, row+1, col, row+count)
        target7 = None
        target8 = None
        if not cell_7 is None:
            col = cell_7.getCellAddress().Column
            target7 = self._sheet.getCellRangeByPosition(col, row+1, col, row+count)
        if pakings:
            col = cell_8.getCellAddress().Column
            target8 = self._sheet.getCellRangeByPosition(col, row+1, col, row+count)

        target1.setFormulaArray(tuple(col1))
        target2.setDataArray(tuple(col2))
        target3.setFormulaArray(tuple(col3))
        target4.setDataArray(tuple(col4))
        target5.setDataArray(tuple(col5))
        target6.setDataArray(tuple(col6))
        if not target7 is None:
            target7.setDataArray(tuple(col7))
        if not target8 is None:
            target8.setDataArray(tuple(col8))

        if style_5:
            cell_5.CellStyle = style_5
            target5.CellStyle = style_5
        if style_6:
            cell_6.CellStyle = style_6
            target6.CellStyle = style_6
        if style_7:
            cell_7.CellStyle = style_7
            target7.CellStyle = style_7
        if style_8:
            cell_8.CellStyle = style_8
            target8.CellStyle = style_8
        return

    def _add_totales(self, data):
        currency = data['moneda']
        value = data['total']
        cell_value = self._set_cell('{total}', value, value=True)
        if cell_value is None:
            return False

        cell_value.CellStyle = currency

        return True

    def _totales(self, data):
        cell_styles = {
            'EUR': 'euro',
        }
        currency = data['moneda']

        self._set_cell('{total_cantidades}', str(self._total_cantidades))

        if self._pagos:
            return

        cell_title = self._set_cell('{subtotal.titulo}', 'SubTotal')
        value = data['subtotal']
        cell_value = self._set_cell('{subtotal}', value, value=True)
        if not cell_value is None:
            cell_value.CellStyle = cell_styles.get(currency, 'peso')

        #~ Si encuentra el campo {total}, se asume que los totales e impuestos
        #~ están declarados de forma independiente cada uno
        if self._add_totales(data):
            return

        #~ Si no se encuentra, copia las celdas hacia abajo de
        #~ {subtotal.titulo} y {subtotal}
        #~ print (data['descuento'])
        if 'descuento' in data:
            self._copy_cell(cell_title)
            self._copy_cell(cell_value)
            cell_title = self._set_cell(v='Descuento', cell=cell_title)
            value = data['descuento']
            cell_value = self._set_cell(v=value, cell=cell_value, value=True)
            cell_value.CellStyle = currency

        for tax in data['traslados']:
            self._copy_cell(cell_title)
            self._copy_cell(cell_value)
            cell_title = self._set_cell(v=tax[0], cell=cell_title)
            cell_value = self._set_cell(v=tax[1], cell=cell_value, value=True)
            cell_value.CellStyle = currency

        for tax in data['retenciones']:
            self._copy_cell(cell_title)
            self._copy_cell(cell_value)
            cell_title = self._set_cell(v=tax[0], cell=cell_title)
            cell_value = self._set_cell(v=tax[1], cell=cell_value, value=True)
            cell_value.CellStyle = currency

        for tax in data['taxlocales']:
            self._copy_cell(cell_title)
            self._copy_cell(cell_value)
            cell_title = self._set_cell(v=tax[0], cell=cell_title)
            cell_value = self._set_cell(v=tax[1], cell=cell_value, value=True)
            cell_value.CellStyle = currency

        self._copy_cell(cell_title)
        self._copy_cell(cell_value)
        cell_title = self._set_cell(v='Total', cell=cell_title)
        value = data['total']
        cell_value = self._set_cell(v=value, cell=cell_value, value=True)
        cell_value.CellStyle = currency
        return

    def _timbre(self, data):
        if self._es_pre or self._is_ticket:
            return

        for k, v in data.items():
            self._set_cell('{timbre.%s}' % k, v)
        pd = self._sheet.getDrawPage()
        image = self._template.createInstance('com.sun.star.drawing.GraphicObjectShape')
        gp = self._create_instance('com.sun.star.graphic.GraphicProvider')
        # ~ image.GraphicURL = data['path_cbb']
        pd.add(image)
        properties = self._set_properties({'URL': self._path_url(data['path_cbb'])})
        image.Graphic = gp.queryGraphic(properties)
        s = Size()
        s.Width = 4150
        s.Height = 4500
        image.setSize(s)
        image.Anchor = self._set_cell('{timbre.cbb}')
        _kill(data['path_cbb'])
        return

    def _donataria(self, data):
        if not data:
            return

        for k, v in data.items():
            self._set_cell('{donataria.%s}' % k, v)
        return

    def _ine(self, data):
        if not data:
            return

        for k, v in data.items():
            self._set_cell('{ine.%s}' % k, v)
        return

    def _divisas(self, data):
        if data:
            for k, v in data.items():
                self._set_cell(f'{{divisas.{k}}}', v)
        return

    def _nomina(self, data):
        if not data:
            return

        percepciones = data.pop('percepciones', [])
        deducciones = data.pop('deducciones', [])
        otrospagos = data.pop('otrospagos', [])
        incapacidades = data.pop('incapacidades', [])

        for k, v in data.items():
            if k.lower() in ('totalpercepciones', 'totaldeducciones',
                'totalotrospagos', 'subsidiocausado'):
                self._set_cell('{nomina.%s}' % k, v, value=True)
            else:
                self._set_cell('{nomina.%s}' % k, v)

        count = len(percepciones)
        if len(deducciones) > count:
            count = len(deducciones)
        count -= 1

        first = True
        separacion = {}
        for r in percepciones:
            if 'TotalPagado' in r:
                separacion = r
                continue

            tipo = r.get('TipoPercepcion')
            concepto = r.get('Concepto')
            gravado = r.get('ImporteGravado')
            exento = r.get('ImporteExento')
            if first:
                first = False
                cell_1 = self._set_cell('{percepcion.TipoPercepcion}', tipo)
                cell_2 = self._set_cell('{percepcion.Concepto}', concepto)
                cell_3 = self._set_cell('{percepcion.ImporteGravado}', gravado, value=True)
                cell_4 = self._set_cell('{percepcion.ImporteExento}', exento, value=True)
                if count:
                    row = cell_1.getCellAddress().Row + 1
                    self._sheet.getRows().insertByIndex(row, count)
                    self._copy_paste_rows(cell_1, count)
                    self._clean_rows(row, count)
            else:
                cell_1 = self._set_cell(v=tipo, cell=cell_1)
                cell_2 = self._set_cell(v=concepto, cell=cell_2)
                cell_3 = self._set_cell(v=gravado, cell=cell_3, value=True)
                cell_4 = self._set_cell(v=exento, cell=cell_4, value=True)

        first = True
        for r in deducciones:
            tipo = r.get('TipoDeduccion')
            concepto = r.get('Concepto')
            importe = r.get('Importe')
            if first:
                first = False
                cell_1 = self._set_cell('{deduccion.TipoDeduccion}', tipo)
                cell_2 = self._set_cell('{deduccion.Concepto}', concepto)
                cell_3 = self._set_cell('{deduccion.Importe}', importe, value=True)
            else:
                cell_1 = self._set_cell(v=tipo, cell=cell_1)
                cell_2 = self._set_cell(v=concepto, cell=cell_2)
                cell_3 = self._set_cell(v=importe, cell=cell_3, value=True)

        count = len(otrospagos) - 1
        first = True
        for r in otrospagos:
            tipo = r.get('TipoOtroPago')
            concepto = r.get('Concepto')
            importe = r.get('Importe')
            if first:
                first = False
                cell_1 = self._set_cell('{otropago.TipoOtroPago}', tipo)
                cell_2 = self._set_cell('{otropago.Concepto}', concepto)
                cell_3 = self._set_cell('{otropago.Importe}', importe, value=True)
                if count:
                    row = cell_1.getCellAddress().Row + 1
                    self._sheet.getRows().insertByIndex(row, count)
                    self._copy_paste_rows(cell_1, count)
                    self._clean_rows(row, count)
            else:
                cell_1 = self._set_cell(v=tipo, cell=cell_1)
                cell_2 = self._set_cell(v=concepto, cell=cell_2)
                cell_3 = self._set_cell(v=importe, cell=cell_3, value=True)

        count = len(incapacidades) - 1
        first = True
        for r in incapacidades:
            tipo = r.get('TipoIncapacidad')
            days = r.get('DiasIncapacidad')
            importe = r.get('ImporteMonetario')
            if first:
                first = False
                cell_1 = self._set_cell('{incapacidad.TipoIncapacidad}', tipo)
                cell_2 = self._set_cell('{incapacidad.DiasIncapacidad}', days)
                cell_3 = self._set_cell('{incapacidad.ImporteMonetario}', importe, value=True)
                # ~ if count:
                    # ~ row = cell_1.getCellAddress().Row + 1
                    # ~ self._sheet.getRows().insertByIndex(row, count)
                    # ~ self._copy_paste_rows(cell_1, count)
                    # ~ self._clean_rows(row, count)
            # ~ else:
                # ~ cell_1 = self._set_cell(v=tipo, cell=cell_1)
                # ~ cell_2 = self._set_cell(v=concepto, cell=cell_2)
                # ~ cell_3 = self._set_cell(v=importe, cell=cell_3, value=True)

        return

    def _cfdipays(self, data):
        related = data.pop('related', [])
        for k, v in data.items():
            if k.lower() in ('monto',):
                self._set_cell('{pago.%s}' % k, v, value=True)
            else:
                self._set_cell('{pago.%s}' % k, v)

        col1 = []
        col2 = []
        col3 = []
        col4 = []
        col5 = []
        col6 = []
        col7 = []
        col8 = []
        col9 = []
        count = len(related)
        for i, doc in enumerate(related):
            uuid = doc['IdDocumento'].upper()
            serie = doc.get('Serie', '')
            folio = doc['Folio']
            metodo_pago = doc['MetodoDePagoDR']
            moneda = doc['MonedaDR']
            parcialidad = doc['NumParcialidad']
            saldo_anterior = doc['ImpSaldoAnt']
            importe_pagado = doc['ImpPagado']
            saldo_insoluto = doc['ImpSaldoInsoluto']
            if i == 0:
                cell_1 = self._set_cell('{doc.uuid}', uuid)
                cell_2 = self._set_cell('{doc.serie}', serie)
                cell_3 = self._set_cell('{doc.folio}', folio)
                cell_4 = self._set_cell('{doc.metodopago}', metodo_pago)
                cell_5 = self._set_cell('{doc.moneda}', moneda)
                cell_6 = self._set_cell('{doc.parcialidad}', parcialidad)
                cell_7 = self._set_cell('{doc.saldoanterior}', saldo_anterior, value=True)
                cell_8 = self._set_cell('{doc.importepagado}', importe_pagado, value=True)
                cell_9 = self._set_cell('{doc.saldoinsoluto}', saldo_insoluto, value=True)
            else:
                col1.append((uuid,))
                col2.append((serie,))
                col3.append((folio,))
                col4.append((metodo_pago,))
                col5.append((moneda,))
                col6.append((parcialidad,))
                col7.append((float(saldo_anterior),))
                col8.append((float(importe_pagado),))
                col9.append((float(saldo_insoluto),))

        if count == 1:
            return

        count -= 1
        row1 = cell_1.getCellAddress().Row + 1
        row2 = row1 + count - 1
        self._sheet.getRows().insertByIndex(row1, count)
        self._copy_paste_rows(cell_1, count)

        # ~ style_7 = self._get_style(cell_7)
        # ~ style_8 = self._get_style(cell_8)
        # ~ style_9 = self._get_style(cell_9)

        col = cell_1.getCellAddress().Column
        target1 = self._sheet.getCellRangeByPosition(col, row1, col, row2)
        col = cell_2.getCellAddress().Column
        target2 = self._sheet.getCellRangeByPosition(col, row1, col, row2)
        col = cell_3.getCellAddress().Column
        target3 = self._sheet.getCellRangeByPosition(col, row1, col, row2)
        col = cell_4.getCellAddress().Column
        target4 = self._sheet.getCellRangeByPosition(col, row1, col, row2)
        col = cell_5.getCellAddress().Column
        target5 = self._sheet.getCellRangeByPosition(col, row1, col, row2)
        col = cell_6.getCellAddress().Column
        target6 = self._sheet.getCellRangeByPosition(col, row1, col, row2)
        col = cell_7.getCellAddress().Column
        target7 = self._sheet.getCellRangeByPosition(col, row1, col, row2)
        col = cell_8.getCellAddress().Column
        target8 = self._sheet.getCellRangeByPosition(col, row1, col, row2)
        col = cell_9.getCellAddress().Column
        target9 = self._sheet.getCellRangeByPosition(col, row1, col, row2)

        target1.setFormulaArray(tuple(col1))
        target2.setDataArray(tuple(col2))
        target3.setFormulaArray(tuple(col3))
        target4.setDataArray(tuple(col4))
        target5.setDataArray(tuple(col5))
        target6.setDataArray(tuple(col6))
        target7.setDataArray(tuple(col7))
        target8.setDataArray(tuple(col8))
        target9.setDataArray(tuple(col9))

        return

    def _render(self, data):
        self._set_search()
        self._es_pre = data.pop('es_pre', False)
        self._is_ticket = data.pop('is_ticket', False)
        self._currency = data['totales']['moneda']
        self._pagos = data.pop('pagos', False)

        pakings = data.pop('pakings', [])

        self._comprobante(data['comprobante'])
        self._emisor(data['emisor'])
        self._receptor(data['receptor'])
        self._conceptos(data['conceptos'], pakings)

        if self._pagos:
            self._cfdipays(data['pays'])

        if 'nomina' in data and data['nomina']:
            self._nomina(data['nomina'])
        else:
            self._totales(data['totales'])
        self._timbre(data['timbre'])
        self._donataria(data['donataria'])
        self._ine(data['ine'])

        self._divisas(data.get('divisas', {}))

        self._cancelado(data['cancelada'])
        self._clean()
        return

    def pdf(self, path, data, ods=False):
        options = {'AsTemplate': True, 'Hidden': True}
        log.debug('Abrir plantilla...')

        self._template = self._doc_open(path, options)
        if self._template is None:
            return b''

        self._template.setPrinter(self._set_properties({'PaperFormat': LETTER}))
        self._render(data)

        path_ods = get_path_temp('.ods')
        self._template.storeToURL(self._path_url(path_ods), ())
        if ods:
            data = self._read(path_ods)
            _kill(path_ods)
            return data

        options = {'FilterName': 'calc_pdf_Export'}
        path_pdf = get_path_temp('.pdf')
        self._template.storeToURL(self._path_url(path_pdf), self._set_properties(options))
        try:
            self._template.close(True)
        except:
            pass
        data = self._read(path_pdf)
        _kill(path_ods)
        _kill(path_pdf)
        return data

    def _get_data(self, doc, name=0):
        try:
            sheet = doc.getSheets()[name]
            cursor = sheet.createCursorByRange(sheet['A1'])
            cursor.collapseToCurrentRegion()
        except KeyError:
            msg = 'Hoja no existe'
            return (), msg
        return cursor.getDataArray(), ''

    def products(self, path):
        options = {'AsTemplate': True, 'Hidden': True}
        doc = self._doc_open(path, options)
        if doc is None:
            return (), 'No se pudo abrir la plantilla'

        data, msg = self._get_data(doc)
        doc.close(True)

        if len(data) == 1:
            msg = 'Sin datos para importar'
            return (), msg

        fields = (
            'categoria',
            'clave',
            'clave_sat',
            'descripcion',
            'unidad',
            'valor_unitario',
            'inventario',
            'existencia',
            'codigo_barras',
            'impuestos',
        )
        rows = [dict(zip(fields, r)) for r in data[1:]]
        return rows, ''

    def employees(self, path):
        options = {'AsTemplate': True, 'Hidden': True}
        doc = self._doc_open(path, options)
        if doc is None:
            return ()

        data, msg = self._get_data(doc, 'Empleados')
        doc.close(True)

        if len(data) == 1:
            msg = 'Sin datos para importar'
            return (), msg

        fields = (
            'num_empleado',
            'rfc',
            'curp',
            'nombre',
            'paterno',
            'materno',
            'fecha_ingreso',
            'imss',
            'tipo_contrato',
            'es_sindicalizado',
            'tipo_jornada',
            'tipo_regimen',
            'departamento',
            'puesto',
            'riesgo_puesto',
            'periodicidad_pago',
            'banco',
            'cuenta_bancaria',
            'clabe',
            'salario_base',
            'salario_diario',
            'estado',
            'codigo_postal',
            'notas',
            'correo',
        )
        rows = tuple([dict(zip(fields, r)) for r in data[1:]])
        msg = 'Empleados importados correctamente'
        return rows, msg

    def _get_nomina(self, doc):
        rows, msg = self._get_data(doc, 'Nomina')
        if len(rows) == 2:
            msg = 'Sin datos para importar'
            return {}, msg

        fields = (
            'rfc',
            'tipo_nomina',
            'fecha_pago',
            'fecha_inicial_pago',
            'fecha_final_pago',
            'relacionados',
            'dias_pagados',
        )
        data = tuple([dict(zip(fields, r[1:])) for r in rows[2:]])
        return data, ''

    def _get_percepciones(self, doc, count):
        rows, msg = self._get_data(doc, 'Percepciones')
        if len(rows) == 2:
            msg = 'Sin Percepciones'
            return {}, msg

        if len(rows[0][2:]) % 2:
            msg = 'Las Percepciones deben ir en pares: Gravado y Exento'
            return {}, msg

        data = tuple([r[2:] for r in rows[:count+2]])
        return data, ''

    def _get_deducciones(self, doc, count):
        rows, msg = self._get_data(doc, 'Deducciones')
        if len(rows) == 2:
            msg = 'Sin Deducciones'
            return {}, msg

        data = tuple([r[2:] for r in rows[:count+2]])

        sheet = doc.Sheets['Deducciones']
        notes = sheet.getAnnotations()
        new_titles = {}
        for n in notes:
            col = n.getPosition().Column - 2
            if data[0][col] == '004':
                new_titles[col] = n.getString()

        return data, new_titles, ''

    def _get_otros_pagos(self, doc, count):
        rows, msg = self._get_data(doc, 'OtrosPagos')
        if len(rows) == 2:
            msg = 'Sin Otros Pagos'
            return {}, msg

        data = tuple([r[2:] for r in rows[:count+2]])
        return data, ''

    def _get_separacion(self, doc, count):
        rows, msg = self._get_data(doc, 'Separacion')
        if len(rows) == 2:
            msg = 'Sin Separacion'
            return {}, msg

        data = tuple([r[1:] for r in rows[:count+2]])
        return data, ''

    def _get_horas_extras(self, doc, count):
        rows, msg = self._get_data(doc, 'HorasExtras')
        if len(rows) == 2:
            msg = 'Sin Horas Extras'
            return {}, msg

        if len(rows[1][1:]) % 4:
            msg = 'Las Horas Extras deben ir grupos de 4 columnas'
            return {}, msg

        data = tuple([r[1:] for r in rows[:count+2]])
        return data, ''

    def _get_incapacidades(self, doc, count):
        rows, msg = self._get_data(doc, 'Incapacidades')
        if len(rows) == 2:
            msg = 'Sin Incapacidades'
            return {}, msg

        if len(rows[1][1:]) % 3:
            msg = 'Las Incapacidades deben ir grupos de 3 columnas'
            return {}, msg

        data = tuple([r[1:] for r in rows[:count+2]])
        return data, ''

    def nomina(self, path):
        options = {'AsTemplate': True, 'Hidden': True}
        doc = self._doc_open(path, options)
        if doc is None:
            msg = 'No se pudo abrir la plantilla'
            return {}, msg

        data = {}
        nomina, msg = self._get_nomina(doc)
        if msg:
            doc.close(True)
            return {}, msg

        percepciones, msg = self._get_percepciones(doc, len(nomina))
        if msg:
            doc.close(True)
            return {}, msg

        deducciones, new_titles, msg = self._get_deducciones(doc, len(nomina))
        if msg:
            doc.close(True)
            return {}, msg

        otros_pagos, msg = self._get_otros_pagos(doc, len(nomina))
        if msg:
            doc.close(True)
            return {}, msg

        separacion, msg = self._get_separacion(doc, len(nomina))
        if msg:
            doc.close(True)
            return {}, msg

        horas_extras, msg = self._get_horas_extras(doc, len(nomina))
        if msg:
            doc.close(True)
            return {}, msg

        incapacidades, msg = self._get_incapacidades(doc, len(nomina))
        if msg:
            doc.close(True)
            return {}, msg

        doc.close(True)

        rows = len(nomina) + 2

        if rows != len(percepciones):
            msg = 'Cantidad de filas incorrecta en: Percepciones'
            return {}, msg
        if rows != len(deducciones):
            msg = 'Cantidad de filas incorrecta en: Deducciones'
            return {}, msg
        if rows != len(otros_pagos):
            msg = 'Cantidad de filas incorrecta en: Otros Pagos'
            return {}, msg
        if rows != len(separacion):
            msg = 'Cantidad de filas incorrecta en: Separación'
            return {}, msg
        if rows != len(horas_extras):
            msg = 'Cantidad de filas incorrecta en: Horas Extras'
            return {}, msg
        if rows != len(incapacidades):
            msg = 'Cantidad de filas incorrecta en: Incapacidades'
            return {}, msg

        data['nomina'] = nomina
        data['percepciones'] = percepciones
        data['deducciones'] = deducciones
        data['otros_pagos'] = otros_pagos
        data['separacion'] = separacion
        data['horas_extras'] = horas_extras
        data['incapacidades'] = incapacidades
        data['new_titles'] = new_titles

        return data, ''

    def invoice(self, path):
        options = {'AsTemplate': True, 'Hidden': True}
        doc = self._doc_open(path, options)
        if doc is None:
            return (), 'No se pudo abrir la plantilla'

        data, msg = self._get_data(doc)
        doc.close(True)

        if len(data) == 1:
            msg = 'Sin datos para importar'
            return (), msg

        rows = tuple(data[1:])
        return rows, ''


def to_pdf(data, emisor_rfc, ods=False, pdf_from='1'):
    rfc = data['emisor']['rfc']
    default = 'plantilla_factura.ods'
    if DEBUG:
        rfc = emisor_rfc
    version = data['comprobante']['version']
    if 'nomina' in data and data['nomina']:
        version = '{}_{}'.format(data['nomina']['version'], version)
        default = 'plantilla_nomina.ods'

    pagos = ''
    if data.get('pagos', False):
        version = '1.0'
        pagos = 'pagos_'

    if pdf_from == '2':
        return to_pdf_from_json(rfc, version, data)

    if APP_LIBO:
        app = LIBO()
        if app.is_running:
            donativo = ''
            if data['donativo']:
                donativo = '_donativo'
            name = '{}_{}{}{}.ods'.format(rfc.lower(), pagos, version, donativo)
            path = get_template_ods(name, default)
            if path:
                return app.pdf(path, data, ods)

    return to_pdf_from_json(rfc, version, data)


def to_pdf_from_json(rfc, version, data):
    rfc = rfc.lower()
    name = '{}_{}.json'.format(rfc, version)
    custom_styles = get_custom_styles(name)

    path_logo = _join(PATHS['LOGOS'], f"{rfc}.png")
    if exists(path_logo):
        data['emisor']['logo'] = path_logo

    path_logo = _join(PATHS['LOGOS'], f"{rfc}_2.png")
    if exists(path_logo):
        data['emisor']['logo2'] = path_logo

    path_cbb = data['timbre']['path_cbb']
    path = get_path_temp()

    pdf = TemplateInvoice(path)
    pdf.custom_styles = custom_styles
    pdf.data = data
    pdf.render()

    data = read_file(path)
    _kill(path)
    _kill(path_cbb)

    return data


def format_currency(value, currency, digits=2):
    c = {
        MXN: '$',
        'USD': '$',
        'EUR': '€',
    }
    s = c.get(currency, MXN)
    return f'{s} {float(value):,.{digits}f}'


def to_html(data):
    name = f"{data['rfc']}_{data['version']}.html"
    try:
        template = template_lookup.get_template(name)
    except TopLevelLookupException:
        template = template_lookup.get_template('plantilla_factura.html')
        data['rfc'] = 'invoice'

    # ~ data['cfdi_sello'] = textwrap.fill(data['cfdi_sello'], 50)
    # ~ data['timbre_sellosat'] = textwrap.fill(data['timbre_sellosat'], 110)
    # ~ data['timbre_cadenaoriginal'] = textwrap.fill(data['timbre_cadenaoriginal'], 140)

    # ~ data['cfdi_sello'] = 'X'*100 + '<BR>' + 'X'*100 + '<BR>' + 'X'*100
    # ~ data['timbre_sellosat'] = 'X'*100 + '<BR>' + 'X'*100 + '<BR>' + 'X'*100

    return template.render(**data)


def html_to_pdf(data):
    path_pdf = '/home/mau/test.pdf'
    css = '/home/mau/projects/empresa-libre/source/static/css/invoice.css'

    # ~ font_config = FontConfiguration()
    # ~ html = HTML(string=data)
    # ~ css = CSS(filename=path_css)
    # ~ html.write_pdf(path_pdf, stylesheets=[css], font_config=font_config)
    options = {
        'page-size': 'Letter',
        'margin-top': '0.50in',
        'margin-right': '0.50in',
        'margin-bottom': '0.50in',
        'margin-left': '0.50in',
        'encoding': "UTF-8",
    }

    pdfkit.from_string(data.decode(), path_pdf, options=options, css=css)

    return


def import_employees(rfc):
    name = '{}_employees.ods'.format(rfc.lower())
    path = _join(PATH_MEDIA, 'tmp', name)
    if not is_file(path):
        return ()

    msg = 'LibreOffice no se pudo iniciar'
    if APP_LIBO:
        app = LIBO()
        if app.is_running:
            return app.employees(path)

    return (), msg


def import_nomina(rfc):
    name = '{}_nomina.ods'.format(rfc.lower())
    path = _join(PATH_MEDIA, 'tmp', name)
    if not is_file(path):
        return ()

    if APP_LIBO:
        app = LIBO()
        if app.is_running:
            return app.nomina(path)

    return ()


def parse_xml(xml):
    try:
        return ET.fromstring(xml)
    except ET.ParseError:
        return None


def to_pretty_xml(xml):
    tree = parseString(xml)
    return tree.toprettyxml(encoding='utf-8').decode('utf-8')


def get_dict(data):
    return CaseInsensitiveDict(data)


def to_letters(value, currency):
    return NumLet(value, currency).letras


def get_qr(data, p=True):
    qr = pyqrcode.create(data, mode='binary')
    if p:
        path = get_path_temp('.qr')
        qr.png(path, scale=7)
        return path

    buffer = io.BytesIO()
    qr.png(buffer, scale=8)
    return base64.b64encode(buffer.getvalue()).decode()


def _get_relacionados(doc, version):
    node = doc.find('{}CfdiRelacionados'.format(PRE[version]))
    if node is None:
        return ''

    uuids = ['UUID: {}'.format(n.attrib['UUID']) for n in node.getchildren()]
    return '\n'.join(uuids)


def _comprobante(doc, options):
    data = CaseInsensitiveDict(doc.attrib.copy())
    del data['certificado']

    serie = ''
    if 'serie' in data:
        serie = '{}-'.format(data['serie'])
    data['seriefolio'] = '{}{}'.format(serie, data.get('folio', ''))
    data['totalenletras'] = to_letters(float(data['total']), data['moneda'])

    is_nomina = options.get('is_nomina', False)
    if is_nomina:
        data['formadepago'] = options['formadepago']
        data['periodicidaddepago'] = options['periodicidaddepago']
        data['tiporelacion'] = options.get('tiporelacion', '')
        return data

    if data['version'] == '3.3':
        tipos = {
            'I': 'ingreso',
            'E': 'egreso',
            'T': 'traslado',
            'P': 'pago',
        }
        data['tipodecomprobante'] = tipos.get(data['tipodecomprobante'])
        data['lugarexpedicion'] = \
            'C.P. de Expedición: {}'.format(data['lugarexpedicion'])
        if 'metododepago' in options:
            data['metododepago'] = options['metododepago']
        if 'formadepago' in options:
            data['formadepago'] = options['formadepago']

        if 'condicionesdepago' in data:
            data['condicionesdepago'] = \
                'Condiciones de pago: {}'.format(data['condicionesdepago'])
        data['moneda'] = options['moneda']
        data['tiporelacion'] = options.get('tiporelacion', '')
        data['relacionados'] = _get_relacionados(doc, data['version'])
    else:
        fields = {
            'formaDePago': 'Forma de Pago: {}\n',
            'metodoDePago': 'Método de pago: {}\n',
            'condicionesDePago': 'Condiciones de Pago: {}\n',
            'NumCtaPago': 'Número de Cuenta de Pago: {}\n',
            'Moneda': 'Moneda: {}\n',
            'TipoCambio': 'Tipo de Cambio: {}',
        }
        datos = ''
        for k, v in fields.items():
            if k in data:
                datos += v.format(data[k])
        data['datos'] = datos

    data['hora'] = data['fecha'].split('T')[1]
    fecha = parser.parse(data['fecha'])
    try:
        locale.setlocale(locale.LC_TIME, "es_MX.UTF-8")
    except:
        pass
    data['fechaformato'] = fecha.strftime('%A, %d de %B de %Y')

    if 'tipocambio' in data:
        data['tipocambio'] = 'Tipo de Cambio: $ {:0.4f}'.format(
            float(data['tipocambio']))
    data['notas'] = options['notas']

    return data


def _emisor(doc, version, values):
    emisor = doc.find('{}Emisor'.format(PRE[version]))
    data = CaseInsensitiveDict(emisor.attrib.copy())
    node = emisor.find('{}DomicilioFiscal'.format(PRE[version]))
    if not node is None:
        data.update(CaseInsensitiveDict(node.attrib.copy()))

    if version == '3.2':
        node = emisor.find('{}RegimenFiscal'.format(PRE[version]))
        if not node is None:
            data['regimenfiscal'] = node.attrib['Regimen']
            data['regimen'] = node.attrib['Regimen']
    else:
        data['regimenfiscal'] = values['regimenfiscal']

    path = _join(PATH_MEDIA, 'logos', '{}.png'.format(data['rfc'].lower()))
    if is_file(path):
        data['logo'] = path

    return data


def _receptor(doc, version, values):
    node = doc.find('{}Receptor'.format(PRE[version]))
    data = CaseInsensitiveDict(node.attrib.copy())
    node = node.find('{}Domicilio'.format(PRE[version]))
    if not node is None:
        data.update(node.attrib.copy())

    if version == '3.2':
        return data

    data['usocfdi'] = values['usocfdi']
    data.update(values['receptor'])
    return data


def _conceptos(doc, version, options):
    is_nomina = options.get('is_nomina', False)

    data = []
    conceptos = doc.find('{}Conceptos'.format(PRE[version]))
    for c in conceptos.getchildren():
        values = CaseInsensitiveDict(c.attrib.copy())
        if is_nomina:
            values['noidentificacion'] = values['ClaveProdServ']
            values['unidad'] = values['ClaveUnidad']
            data.append(values)
            continue

        if version == '3.3':
            if 'noidentificacion' in values:
                values['noidentificacion'] = '{}\n(SAT {})'.format(
                    values['noidentificacion'], values['ClaveProdServ'])
            else:
                values['noidentificacion'] = 'SAT {}'.format(
                    values['ClaveProdServ'])
            if 'unidad' in values:
                values['unidad'] = '({})\n{}'.format(
                    values['ClaveUnidad'], values['unidad'])
            else:
                values['unidad'] = '{}'.format(values['ClaveUnidad'])

        n = c.find('{}CuentaPredial'.format(PRE[version]))
        if n is not None:
            v = CaseInsensitiveDict(n.attrib.copy())
            info = '\nCuenta Predial Número: {}'.format(v['numero'])
            values['descripcion'] += info

        n = c.find('{}InformacionAduanera'.format(PRE[version]))
        if n is not None:
            v = CaseInsensitiveDict(n.attrib.copy())
            info = '\nNúmero Pedimento: {}'.format(v['numeropedimento'])
            values['descripcion'] += info

        n = c.find('{}ComplementoConcepto'.format(PRE[version]))
        if n is not None:
            v = CaseInsensitiveDict(n[0].attrib.copy())
            info = '\nAlumno: {} (CURP: {})\nNivel: {}, Autorización: {}'.format(
                v['nombreAlumno'], v['CURP'], v['nivelEducativo'], v['autRVOE'])
            values['descripcion'] += info

        data.append(values)
    return data


def _totales(doc, cfdi, version):
    data = {}
    data['moneda'] = doc.attrib['Moneda']
    data['subtotal'] = cfdi['subtotal']
    if 'descuento' in cfdi:
        data['descuento'] = cfdi['descuento']
    data['total'] = cfdi['total']

    tn = {
        '001': 'ISR',
        '002': 'IVA',
        '003': 'IEPS',
    }
    traslados = []
    retenciones = []
    taxlocales = []

    imp = doc.find('{}Impuestos'.format(PRE[version]))
    if imp is not None:
        tmp = CaseInsensitiveDict(imp.attrib.copy())
        for k, v in tmp.items():
            data[k] = v

        node = imp.find('{}Traslados'.format(PRE[version]))
        if node is not None:
            for n in node.getchildren():
                tmp = CaseInsensitiveDict(n.attrib.copy())
                if version == '3.3':
                    tasa = round(float(tmp['tasaocuota']), DECIMALES)
                    title = 'Traslado {} {}'.format(tn.get(tmp['impuesto']), tasa)
                else:
                    title = 'Traslado {} {}'.format(tmp['impuesto'], tmp['tasa'])
                traslados.append((title, float(tmp['importe'])))

        node = imp.find('{}Retenciones'.format(PRE[version]))
        if node is not None:
            for n in node.getchildren():
                tmp = CaseInsensitiveDict(n.attrib.copy())
                if version == '3.3':
                    title = 'Retención {} {}'.format(
                        tn.get(tmp['impuesto']), '')
                else:
                    title = 'Retención {} {}'.format(tmp['impuesto'], '')
                retenciones.append((title, float(tmp['importe'])))

    node = doc.find('{}Complemento/{}ImpuestosLocales'.format(
        PRE[version], PRE['LOCALES']))
    if node is not None:
        for otro in list(node):
            if otro.tag == '{}RetencionesLocales'.format(PRE['LOCALES']):
                tipo = 'Retención '
                name = 'ImpLocRetenido'
                tasa = 'TasadeRetencion'
            else:
                tipo = 'Traslado '
                name = 'ImpLocTrasladado'
                tasa = 'TasadeTraslado'
            title = '{} {} {}%'.format(
                tipo, otro.attrib[name], otro.attrib[tasa])
            importe = float(otro.attrib['Importe'])
            taxlocales.append((title, importe))

    data['traslados'] = traslados
    data['retenciones'] = retenciones
    data['taxlocales'] = taxlocales
    return data


def _timbre(doc, version, values):
    CADENA = '||{version}|{UUID}|{FechaTimbrado}|{selloCFD}|{noCertificadoSAT}||'
    if version == '3.3':
        CADENA = '||{Version}|{UUID}|{FechaTimbrado}|{SelloCFD}|{NoCertificadoSAT}||'
    node = doc.find('{}Complemento/{}TimbreFiscalDigital'.format(
        PRE[version], PRE['TIMBRE']))
    data = CaseInsensitiveDict(node.attrib.copy())

    qr_data = {
        'url': 'https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?',
        'uuid': '&id={}'.format(data['uuid']),
        'emisor': '&re={}'.format(values['rfc_emisor']),
        'receptor': '&rr={}'.format(values['rfc_receptor']),
        'total': '&tt={}'.format(values['total']),
        'sello': '&fe={}'.format(data['sellocfd'][-8:]),
    }
    qr_data = '{url}{uuid}{emisor}{receptor}{total}{sello}'.format(**qr_data)

    data['path_cbb'] = get_qr(qr_data)
    data['cadenaoriginal'] = CADENA.format(**data)
    return data


def _donataria(doc, version, fechadof):
    node = doc.find('{}Complemento/{}Donatarias'.format(
        PRE[version], PRE['DONATARIA']))
    if node is None:
        return {}

    data = CaseInsensitiveDict(node.attrib.copy())
    data['fechadof'] = fechadof
    return data


def _ine(doc, version):
    node = doc.find('{}Complemento/{}INE'.format(PRE[version], PRE['INE']))
    if node is None:
        return {}

    values = (
        ('TipoComite', 'Tipo de Comite: {}'),
        ('TipoProceso', 'Tipo de Proceso: {}'),
        ('IdContabilidad', 'ID de Contabilidad: {}'),
    )
    data = CaseInsensitiveDict(node.attrib.copy())
    for k, v in values:
        data[k] = v.format(data[k])
    return data


def _nomina(doc, data, values, version_cfdi):
    is_nomina = values.get('is_nomina', False)
    if not is_nomina:
        return {}

    version = values['version']

    node_nomina = doc.find('{}Complemento/{}Nomina'.format(
        PRE[version_cfdi], PRE['NOMINA'][version]))
    if node_nomina is None:
        return {}
    info = CaseInsensitiveDict(node_nomina.attrib.copy())

    node = node_nomina.find('{}Emisor'.format(PRE['NOMINA'][version]))
    if not node is None:
        data['emisor'].update(CaseInsensitiveDict(node.attrib.copy()))

    node = node_nomina.find('{}Receptor'.format(PRE['NOMINA'][version]))
    data['receptor'].update(CaseInsensitiveDict(node.attrib.copy()))

    node = node_nomina.find('{}Percepciones'.format(PRE['NOMINA'][version]))
    if not node is None:
        data['comprobante'].update(CaseInsensitiveDict(node.attrib.copy()))
        info['percepciones'] = []
        for p in node.getchildren():
            info['percepciones'].append(CaseInsensitiveDict(p.attrib.copy()))

    node = node_nomina.find('{}Deducciones'.format(PRE['NOMINA'][version]))
    if not node is None:
        data['comprobante'].update(CaseInsensitiveDict(node.attrib.copy()))
        info['deducciones'] = []
        for d in node.getchildren():
            info['deducciones'].append(CaseInsensitiveDict(d.attrib.copy()))

    node = node_nomina.find('{}OtrosPagos'.format(PRE['NOMINA'][version]))
    if not node is None:
        info['otrospagos'] = []
        for o in node.getchildren():
            info['otrospagos'].append(CaseInsensitiveDict(o.attrib.copy()))
            n = o.find('{}SubsidioAlEmpleo'.format(PRE['NOMINA'][version]))
            if not n is None:
                info.update(CaseInsensitiveDict(n.attrib.copy()))

    node = node_nomina.find('{}Incapacidades'.format(PRE['NOMINA'][version]))
    if not node is None:
        info['incapacidades'] = []
        for i in node.getchildren():
            info['incapacidades'].append(CaseInsensitiveDict(i.attrib.copy()))

    return info


def _cfdipays(doc, data, version):
    node = doc.find('{}Complemento/{}Pagos'.format(PRE[version], PRE['pagos']))
    if node is None:
        return {}

    info = CaseInsensitiveDict(node.attrib.copy())
    related = []
    for n1 in node:
        info.update(CaseInsensitiveDict(n1.attrib.copy()))
        for n2 in n1:
            related.append(CaseInsensitiveDict(n2.attrib.copy()))

    info['related'] = related

    data['comprobante']['totalenletras'] = to_letters(
        float(info['monto']), info['monedap'])
    data['comprobante']['moneda'] = info['monedap']

    return info


def get_data_from_xml(invoice, values):
    data = {'cancelada': invoice.cancelada, 'donativo': False}
    if hasattr(invoice, 'donativo'):
        data['donativo'] = invoice.donativo
    doc = parse_xml(invoice.xml)
    data['comprobante'] = _comprobante(doc, values)
    version = data['comprobante']['version']
    data['emisor'] = _emisor(doc, version, values)
    data['receptor'] = _receptor(doc, version, values)
    data['conceptos'] = _conceptos(doc, version, values)
    data['totales'] = _totales(doc, data['comprobante'], version)
    data['donataria'] = _donataria(doc, version, values['fechadof'])
    data['ine'] = _ine(doc, version)

    options = {
        'rfc_emisor': data['emisor']['rfc'],
        'rfc_receptor': data['receptor']['rfc'],
        'total': data['comprobante']['total'],
    }
    data['timbre'] = _timbre(doc, version, options)
    del data['timbre']['version']
    data['comprobante'].update(data['timbre'])

    data['nomina'] = _nomina(doc, data, values, version)
    data['pagos'] = values.get('pagos', False)
    if data['pagos']:
        data['pays'] = _cfdipays(doc, data, version)
    data['pakings'] = values.get('pakings', [])
    return data


def to_zip(*files):
    zip_buffer = BytesIO()

    with zipfile.ZipFile(zip_buffer, 'a', zipfile.ZIP_DEFLATED, False) as zip_file:
        for data, file_name in files:
            zip_file.writestr(file_name, data)

    return zip_buffer.getvalue()


def make_fields(xml):
    doc = ET.fromstring(xml)
    data = CaseInsensitiveDict(doc.attrib.copy())
    data.pop('certificado')
    data.pop('sello')
    version = data['version']
    receptor = doc.find('{}Receptor'.format(PRE[version]))
    receptor = CaseInsensitiveDict(receptor.attrib.copy())
    data['receptor_nombre'] = receptor['nombre']
    data['receptor_rfc'] = receptor['rfc']
    data = {k.lower(): v for k, v in data.items()}
    return data


def make_info_mail(data, fields):
    try:
        return data.format(**fields).replace('\n', '<br/>')
    except:
        log.error(data)
        log.error(fields)
        return data.replace('\n', '<br/>')


def send_mail(data):
    msg = ''
    server = SendMail(data['server'])
    is_connect = server.is_connect
    if is_connect:
        msg = server.send(data['options'])
    else:
        msg = server.error
    server.close()
    return {'ok': is_connect, 'msg': msg}


def exists(path):
    return os.path.exists(path)


def get_path_info(path):
    path, filename = os.path.split(path)
    name, extension = os.path.splitext(filename)
    return (path, filename, name, extension)


def get_path_temp(s=''):
    return tempfile.mkstemp(s)[1]


def get_date(value, next_day=False):
    d = parser.parse(value)
    if next_day:
        return d + datetime.timedelta(days=1)
    return d


class UpFile(object):


    def __init__(self):
        self._init_values()

    def _init_values(self):

        return


def upload_file(rfc, opt, file_obj):
    rfc = rfc.lower()
    tmp = file_obj.filename.split('.')
    ext = tmp[-1].lower()

    EXTENSIONS = {
        'txt_plantilla_factura_32': EXT['ODS'],
        'txt_plantilla_factura_33': EXT['ODS'],
        'txt_plantilla_factura_html': EXT['HTML'],
        'txt_plantilla_factura_css': EXT['CSS'],
        'txt_plantilla_factura_json': EXT['JSON'],
    }

    if opt in EXTENSIONS:
        if ext != EXTENSIONS[opt]:
            msg = (
                f"Extensión de archivo incorrecta, "
                f"selecciona un archivo {EXTENSIONS[opt].upper()}"
            )
            return {'status': 'server', 'name': msg, 'ok': False}

        NAMES = {
            'txt_plantilla_factura_32': f"{rfc}_3.2.ods",
            'txt_plantilla_factura_33': f"{rfc}_3.3.ods",
            'txt_plantilla_factura_html': f"{rfc}_3.3.html",
            'txt_plantilla_factura_css': f"{rfc}.css",
            'txt_plantilla_factura_json': f"{rfc}_3.3.json",
        }
        name = NAMES[opt]
        paths = {
            'txt_plantilla_factura_32': _join(PATHS['USER'], name),
            'txt_plantilla_factura_33': _join(PATHS['USER'], name),
            'txt_plantilla_factura_html': _join(PATHS['USER'], name),
            'txt_plantilla_factura_css': _join(PATHS['CSS'], name),
            'txt_plantilla_factura_json': _join(PATHS['USER'], name),
        }
        if save_file(paths[opt], file_obj.file.read()):
            return {'status': 'server', 'name': file_obj.filename, 'ok': True}
        return {'status': 'error', 'ok': False}

    if opt == 'txt_plantilla_ticket':
        tmp = file_obj.filename.split('.')
        ext = tmp[-1].lower()
        if ext != 'ods':
            msg = 'Extensión de archivo incorrecta, selecciona un archivo ODS'
            return {'status': 'server', 'name': msg, 'ok': False}

        name = '{}_ticket.ods'.format(rfc.lower())
        path = _join(PATH_MEDIA, 'templates', name)
    elif opt == 'txt_plantilla_donataria':
        tmp = file_obj.filename.split('.')
        ext = tmp[-1].lower()
        if ext != 'ods':
            msg = 'Extensión de archivo incorrecta, selecciona un archivo ODS'
            return {'status': 'server', 'name': msg, 'ok': False}

        name = '{}_3.3_donativo.ods'.format(rfc.lower())
        path = _join(PATH_MEDIA, 'templates', name)
    elif opt == 'txt_plantilla_nomina1233':
        tmp = file_obj.filename.split('.')
        ext = tmp[-1].lower()
        if ext != 'ods':
            msg = 'Extensión de archivo incorrecta, selecciona un archivo ODS'
            return {'status': 'server', 'name': msg, 'ok': False}

        name = '{}_1.2_3.3.ods'.format(rfc.lower())
        path = _join(PATH_MEDIA, 'templates', name)
    elif opt == 'txt_plantilla_pagos10':
        tmp = file_obj.filename.split('.')
        ext = tmp[-1].lower()
        if ext != 'ods':
            msg = 'Extensión de archivo incorrecta, selecciona un archivo ODS'
            return {'status': 'server', 'name': msg, 'ok': False}

        name = '{}_pagos_1.0.ods'.format(rfc.lower())
        path = _join(PATH_MEDIA, 'templates', name)
    elif opt == 'bdfl':
        tmp = file_obj.filename.split('.')
        ext = tmp[-1].lower()
        if ext != 'sqlite':
            msg = 'Extensión de archivo incorrecta, selecciona un archivo SQLite'
            return {'status': 'server', 'name': msg, 'ok': False}

        name = '{}.sqlite'.format(rfc.lower())
        path = _join('/tmp', name)
    elif opt == 'products':
        tmp = file_obj.filename.split('.')
        ext = tmp[-1].lower()
        if ext != 'ods':
            msg = 'Extensión de archivo incorrecta, selecciona un archivo ODS'
            return {'status': 'server', 'name': msg, 'ok': False}

        name = '{}_products.ods'.format(rfc.lower())
        path = _join(PATH_MEDIA, 'tmp', name)
    elif opt == 'invoiceods':
        tmp = file_obj.filename.split('.')
        ext = tmp[-1].lower()
        if ext != 'ods':
            msg = 'Extensión de archivo incorrecta, selecciona un archivo ODS'
            return {'status': 'server', 'name': msg, 'ok': False}

        name = '{}_invoice.ods'.format(rfc.lower())
        path = _join(PATH_MEDIA, 'tmp', name)
    elif opt == 'employees':
        tmp = file_obj.filename.split('.')
        ext = tmp[-1].lower()
        if ext != 'ods':
            msg = 'Extensión de archivo incorrecta, selecciona un archivo ODS'
            return {'status': 'server', 'name': msg, 'ok': False}

        name = '{}_employees.ods'.format(rfc.lower())
        path = _join(PATH_MEDIA, 'tmp', name)
    elif opt == 'nomina':
        tmp = file_obj.filename.split('.')
        ext = tmp[-1].lower()
        if ext != 'ods':
            msg = 'Extensión de archivo incorrecta, selecciona un archivo ODS'
            return {'status': 'server', 'name': msg, 'ok': False}

        name = '{}_nomina.ods'.format(rfc.lower())
        path = _join(PATH_MEDIA, 'tmp', name)

    if save_file(path, file_obj.file.read()):
        return {'status': 'server', 'name': file_obj.filename, 'ok': True}

    return {'status': 'error', 'ok': False}


def _get_pem_from_pfx(cert):
    tmp_p12 = save_temp(cert.p12)
    args = "openssl pkcs12 -in '{}' -clcerts -nodes -nocerts " \
        "-passin pass:'{}' | openssl rsa".format(tmp_p12, _get_md5(cert.rfc))
    result = _call(args)
    _kill(tmp_p12)
    return result.encode()


def cancel_xml(auth, uuid, certificado):
    from .pac import Finkok as PAC

    if DEBUG:
        auth = {}
    else:
        if not auth:
            msg = 'Sin datos para cancelar'
            data = {'ok': False, 'error': msg}
            return data, result

    msg = 'Factura cancelada correctamente'
    data = {'ok': True, 'msg': msg, 'row': {'estatus': 'Cancelada'}}
    pac = PAC(auth)
    result = pac.cancel_xml(certificado.rfc, str(uuid).upper(),
        certificado.cer_pem.encode(), _get_pem_from_pfx(certificado))
    if result:
        codes = {None: '',
            'Could not get UUID Text': 'UUID no encontrado',
            'Invalid Passphrase': 'Contraseña inválida',
        }
        if not result['CodEstatus'] is None:
            data['ok'] = False
            data['msg'] = codes.get(result['CodEstatus'], result['CodEstatus'])
    else:
        data['ok'] = False
        data['msg'] = pac.error

    return data, result


def cancel_signature(uuid, pk12, rfc, auth):
    from .pac import Finkok as PAC

    token =  _get_md5(rfc)
    if USAR_TOKEN:
        token = auth['PASS']
        if AUTH['DEBUG']:
            token = AUTH['PASS']

    template = read_file(TEMPLATE_CANCEL, 'r')
    data = {
        'rfc': rfc,
        'fecha': datetime.datetime.now().isoformat()[:19],
        'uuid': str(uuid).upper(),
    }
    template = template.format(**data)

    data = {
        'xmlsec': PATH_XMLSEC,
        'pk12': save_temp(pk12),
        'pass': token,
        'template': save_temp(template, 'w'),
    }
    args = '"{xmlsec}" --sign --pkcs12 "{pk12}" --pwd {pass} ' \
        '"{template}"'.format(**data)
    xml_sign = _call(args)

    if DEBUG:
        auth = {}
    else:
        if not auth:
            msg = 'Sin datos para cancelar'
            result = {'ok': False, 'error': msg}
            return result

    msg = 'Factura cancelada correctamente'
    data = {'ok': True, 'msg': msg, 'row': {'estatus': 'Cancelada'}}
    pac = PAC(auth)
    result = pac.cancel_signature(xml_sign)
    if result:
        codes = {None: '',
            'Could not get UUID Text': 'UUID no encontrado'}
        if not result['CodEstatus'] is None:
            data['ok'] = False
            data['msg'] = codes.get(result['CodEstatus'], result['CodEstatus'])
    else:
        data['ok'] = False
        data['msg'] = pac.error

    return data, result


def run_in_thread(fn):
    def run(*k, **kw):
        t = threading.Thread(target=fn, args=k, kwargs=kw)
        t.start()
        return t
    return run


def get_bool(value):
    if not value:
        return False
    if value == '1':
        return True
    return False


def get_float(value, four=False):
    if four:
        return round(float(value), DECIMALES_TAX)
    return round(float(value), DECIMALES)


def crear_rol(user, contra=''):
    if not contra:
        contra = user
    args = 'psql -U postgres -c "CREATE ROLE {} WITH LOGIN ENCRYPTED ' \
        'PASSWORD \'{}\';"'.format(user, contra)
    try:
        result = _call(args)
        if result == 'CREATE ROLE\n':
            return True
    except Exception as e:
        log.info(e)

    return False


def crear_db(nombre):
    args = 'psql -U postgres -c "CREATE DATABASE {0} WITH ' \
        'OWNER {0};"'.format(nombre)
    try:
        result = _call(args)
        print (result)
        if result == 'CREATE DATABASE\n':
            return True
    except Exception as e:
        log.info(e)

    return False


def _backup_db(user):
    dt = datetime.datetime.now().strftime('%y%m%d_%H%M')
    path_bk = _join(PATH_MEDIA, 'tmp', '{}_{}.bk'.format(user, dt))
    args = 'pg_dump -U postgres -Fc {} > "{}"'.format(user, path_bk)
    _call(args)
    return


def delete_db(user, bk=True):
    if bk:
        _backup_db(user)
    args = 'psql -U postgres -c "DROP DATABASE {0};"'.format(user)
    _call(args)
    args = 'psql -U postgres -c "DROP ROLE {0};"'.format(user)
    _call(args)
    return


def _to_seafile(path_db, data):
    if DEBUG:
        return

    _, filename = os.path.split(path_db)
    if SEAFILE_SERVER:
        msg = '\tSincronizando backup general...'
        log.info(msg)
        seafile = SeaFileAPI(
            SEAFILE_SERVER['URL'],
            SEAFILE_SERVER['USER'],
            SEAFILE_SERVER['PASS'])

        if seafile.is_connect:
            msg = '\tSincronizando: {} '.format(filename)
            log.info(msg)
            seafile.update_file(
                path_db, SEAFILE_SERVER['REPO'], '/', SEAFILE_SERVER['PASS'])
            msg = '\tRespaldo general de {} sincronizado'.format(filename)
            log.info(msg)

    msg = '\tSin datos para sincronización particular de {}'.format(filename)
    if len(data) < 2:
        log.info(msg)
        return
    if not data[0] or not data[1] or not data[2]:
        log.info(msg)
        return

    msg = '\tSincronizando backup particular...'
    log.info(msg)
    seafile = SeaFileAPI(SEAFILE_SERVER['URL'], data[0], data[1])
    if seafile.is_connect:
        msg = '\t\tSincronizando: {} '.format(filename)
        log.info(msg)
        seafile.update_file(path_db, data[2], 'Base de datos/', data[1])
        msg = '\t\tRespaldo partícular de {} sincronizado'.format(filename)
        log.info(msg)

    return


@run_in_thread
def _backup_and_sync(rfc, data):
    msg = 'Generando backup de: {}'.format(rfc)
    log.info(msg)

    sql = 'select correo_timbrado, token_timbrado, token_soporte from emisor;'
    path_bk = _join(PATH_MEDIA, 'tmp', '{}.bk'.format(rfc.lower()))
    if data['type'] == 'postgres':
        args = 'pg_dump -U postgres -Fc {} > "{}"'.format(
            data['name'], path_bk)
        sql = 'psql -U postgres -d {} -Atc "{}"'.format(data['name'], sql)
    elif data['type'] == 'sqlite':
        args = 'gzip -c "{}" > "{}"'.format(data['name'], path_bk)
        sql = 'sqlite3 "{}" "{}"'.format(data['name'], sql)
    try:
        result = _call(args)
        msg = '\tBackup generado de {}'.format(rfc)
        log.info(msg)
        result = _call(sql).strip().split('|')
        _to_seafile(path_bk, result)
    except Exception as e:
        log.info(e)

    return


@run_in_thread
def _backup_companies():
    if DEBUG:
        return

    _, filename = os.path.split(COMPANIES)
    if SEAFILE_SERVER:
        msg = '\tSincronizando backup RFCs...'
        log.info(msg)
        seafile = SeaFileAPI(
            SEAFILE_SERVER['URL'],
            SEAFILE_SERVER['USER'],
            SEAFILE_SERVER['PASS'])

        if seafile.is_connect:
            msg = '\tSincronizando: {} '.format(filename)
            log.info(msg)
            seafile.update_file(
                COMPANIES, SEAFILE_SERVER['REPO'], '/', SEAFILE_SERVER['PASS'])
            msg = '\tRespaldo general de {} sincronizado'.format(filename)
            log.info(msg)
    return


def backup_dbs():
    con = sqlite3.connect(COMPANIES)
    cursor = con.cursor()
    sql = "SELECT * FROM names"
    cursor.execute(sql)
    rows = cursor.fetchall()
    if rows is None:
        return
    cursor.close()
    con.close()

    for rfc, data in rows:
        args = loads(data)
        _backup_and_sync(rfc, args)

    _backup_companies()
    return


def _validar_directorios(path_bk, target):
    path = Path(_join(path_bk, target))
    path.mkdir(parents=True, exist_ok=True)
    return str(path)


def local_copy(files):
    if not MV:
        return

    path_bk = _join(str(Path.home()), DIR_FACTURAS)
    if not os.path.isdir(path_bk):
        msg = 'No existe la carpeta: facturas'
        log.error(msg)
        return

    args = 'df -P {} | tail -1 | cut -d" " -f 1'.format(path_bk)
    try:
        result = _call(args)
        # ~ log.info(result)
    except:
        pass
        # ~ if result != 'empresalibre\n':
            # ~ log.info(result)
            # ~ msg = 'Asegurate de que exista la carpeta para sincronizar'
            # ~ log.error(msg)
            # ~ return
    # ~ except subprocess.CalledProcessError:
        # ~ msg = 'No se pudo obtener la ruta para sincronizar'
        # ~ log.error(msg)
        # ~ return

    try:
        for obj, name, target in files:
            path = _validar_directorios(path_bk, target)
            path_file = _join(path, name)
            m = 'wb'
            if name.endswith('xml'):
                m = 'w'
            save_file(path_file, obj, m)
    except Exception as e:
        log.error(e)

    return


def sync_files(files, auth={}):
    if not MV:
        return

    path_bk = _join(str(Path.home()), DIR_FACTURAS)
    if not os.path.isdir(path_bk):
        msg = 'No existe la carpeta: facturas'
        log.error(msg)
        return

    for obj, name, target in files:
        path = _validar_directorios(path_bk, target)
        path_file = _join(path, name)
        m = 'wb'
        if name.endswith('xml'):
            m = 'w'
        save_file(path_file, obj, m)
    return


def sync_cfdi(auth, files):
    local_copy(files)

    if DEBUG:
        return

    if not auth['REPO'] or not SEAFILE_SERVER:
        return

    seafile = SeaFileAPI(SEAFILE_SERVER['URL'], auth['USER'], auth['PASS'])
    if seafile.is_connect:
        for f in files:
            seafile.update_file(
                f, auth['REPO'], 'Facturas/{}/'.format(f[2]), auth['PASS'])
    return


class ImportFacturaLibreGambas(object):

    def __init__(self, conexion, rfc):
        self._rfc = rfc
        self._con = None
        self._cursor = None
        self._error = ''
        self._is_connect = self._connect(conexion)
        self._clientes = []
        self._clientes_rfc = []

    @property
    def error(self):
        return self._error

    @property
    def is_connect(self):
        return self._is_connect

    def _validate_rfc(self):
        sql = "SELECT rfc FROM emisor LIMIT 1"
        self._cursor.execute(sql)
        obj = self._cursor.fetchone()
        if obj is None:
            self._error = 'No se encontró al emisor: {}'.format(self._rfc)
            return False

        if not DEBUG:
            if obj['rfc'] != self._rfc:
                self._error = 'Los datos no corresponden al RFC: {}'.format(self._rfc)
                return False

        return True

    def _connect(self, conexion):
        import psycopg2
        import psycopg2.extras
        try:
            self._con = psycopg2.connect(conexion)
            self._cursor = self._con.cursor(cursor_factory=psycopg2.extras.DictCursor)
            return self._validate_rfc()
        except Exception as e:
            log.error(e)
            self._error = 'No se pudo conectar a la base de datos'
            return False

    def close(self):
        try:
            self._cursor.close()
            self._con.close()
        except:
            pass
        return

    def import_data(self):
        data = {}
        tables = (
            ('receptores', 'Socios'),
            ('cfdifacturas', 'Facturas'),
            ('categorias', 'Categorias'),
            ('productos', 'Productos'),
            ('tickets', 'Tickets'),
        )
        for source, target in tables:
            data[target] = self._get_table(source)

        data['Socios'] += self._clientes

        return data

    def _get_table(self, table):
        return getattr(self, '_{}'.format(table))()

    def _tickets(self):
        sql = "SELECT * FROM tickets"
        self._cursor.execute(sql)
        rows = self._cursor.fetchall()

        fields = (
            ('serie', 'serie'),
            ('folio', 'folio'),
            ('fecha', 'fecha'),
            ('formadepago', 'forma_pago'),
            ('subtotal', 'subtotal'),
            ('descuento', 'descuento'),
            ('total', 'total'),
            ('notas', 'notas'),
            ('factura', 'factura'),
            ('cancelada', 'cancelado'),
            ('vendedor', 'vendedor'),
        )
        data = []
        totals = len(rows)
        for i, row in enumerate(rows):
            msg = '\tImportando ticket {} de {}'.format(i+1, totals)
            log.info(msg)

            new = {t: row[s] for s, t in fields}

            new['notas'] = ''
            new['fecha'] = new['fecha'].replace(microsecond=0)
            new['estatus'] = 'Generado'
            if new['cancelado']:
                new['estatus'] = 'Cancelado'
            new['factura'] = self._get_invoice_ticket(new['factura'])

            new['details'] = self._get_details_ticket(row['id'])
            new['taxes'] = self._get_taxes_ticket(row['id'])
            data.append(new)

        return data

    def _get_invoice_ticket(self, invoice):
        if not invoice:
            return None

        sql = "SELECT serie, folio FROM cfdifacturas WHERE id=%s"
        self._cursor.execute(sql, [invoice])
        row = self._cursor.fetchone()
        if row is None:
            return {}

        return dict(row)

    def _get_details_ticket(self, id):
        sql = "SELECT * FROM t_detalle WHERE id_cfdi=%s"
        self._cursor.execute(sql, [id])
        rows = self._cursor.fetchall()

        fields = (
            ('descripcion', 'descripcion'),
            ('cantidad', 'cantidad'),
            ('valorunitario', 'valor_unitario'),
            ('importe', 'importe'),
            ('precio', 'precio_final'),
        )

        data = []
        for row in rows:
            new = {t: row[s] for s, t in fields if row[s]}
            data.append(new)

        return data

    def _get_taxes_ticket(self, id):
        sql = "SELECT * FROM t_impuestos WHERE id_cfdi=%s"
        self._cursor.execute(sql, [id])
        rows = self._cursor.fetchall()

        tasas = {
            '0': 0.0,
            '16': 0.16,
            '16.00': 0.16,
            '0.16': 0.16,
            '11': 0.11,
            '-10': 0.10,
            '-2': 0.02,
            '-0.5': 0.005,
            '-2/3': 0.106667,
            '-10.6667': 0.106667,
            '-10.6666': 0.106667,
            '-10.666666': 0.106667,
            '-10.66660': 0.106667,
        }

        data = []
        for row in rows:
            filtro = {
                'name': row['impuesto'],
                'tasa': tasas[row['tasa']],
                'tipo': row['tipo'][0],
            }
            new = {
                'import': row['importe'],
                'filter': filtro
            }
            data.append(new)

        return data

    def _productos(self):
        UNIDADES = {
            'k': 'KGM',
            'kg': 'KGM',
            'kg.': 'KGM',
            'pieza': 'H87',
            'pza': 'H87',
            'pz': 'H87',
            'bulto': 'H87',
            'b': 'H87',
            'exb': 'H87',
            'ex': 'H87',
            'caja': 'XBX',
            'c': 'XBX',
            'rollo': 'XRO',
            'tira': 'SR',
            't': 'SR',
            'cono': 'XAJ',
            'paquete': 'XPK',
            'pq': 'XPK',
        }
        sql = "SELECT * FROM productos"
        self._cursor.execute(sql)
        rows = self._cursor.fetchall()

        fields = (
            ('id_categoria', 'categoria'),
            ('noidentificacion', 'clave'),
            ('descripcion', 'descripcion'),
            # ~ ('unidad', 'unidad'),
            ('id_unidad', 'unidad'),
            # ~ ('costo', 'ultimo_costo'),
            ('valorunitario', 'valor_unitario'),
            # ~ ('existencia', 'existencia'),
            # ~ ('minimo', 'minimo'),
            ('inventario', 'inventario'),
            ('codigobarras', 'codigo_barras'),
            ('cuentapredial', 'cuenta_predial'),
        )
        data = []

        sql = """
            SELECT nombre, tasa, tipo
            FROM impuestos
            WHERE id=%s
        """
        totals = len(rows)
        for i, row in enumerate(rows):
            msg = '\tImportando producto {} de {}'.format(i+1, totals)
            log.info(msg)
            # ~ print (i, dict(row))
            new = {t: row[s] for s, t in fields}

            # ~ print (new['unidad'])
            if new['unidad'] == 2:
                new['unidad'] = 'servicio'

            u = new['unidad'].lower().strip()
            if u in ('sin',):
                continue
            if not u:
                u = 'pieza'

            if not new['categoria']:
                new['categoria'] = None
            new['codigo_barras'] = new['codigo_barras'] or ''
            new['cuenta_predial'] = new['cuenta_predial'] or ''
            new['descripcion'] = ' '.join(new['descripcion'].split())
            new['clave_sat'] = DEFAULT_SAT_PRODUCTO

            new['unidad'] = UNIDADES.get(u, new['unidad'])
            self._cursor.execute(sql, [row['id_impuesto1']])
            impuestos = self._cursor.fetchall()
            new['impuestos'] = tuple(impuestos)
            data.append(new)

        return data

    def _categorias(self):
        sql = "SELECT * FROM categorias ORDER BY id_padre"
        self._cursor.execute(sql)
        rows = self._cursor.fetchall()

        fields = (
            ('id', 'id'),
            ('categoria', 'categoria'),
            ('id_padre', 'padre'),
        )
        data = []

        for row in rows:
            new = {t: row[s] for s, t in fields}
            if new['padre'] == 0:
                new['padre'] = None
            data.append(new)

        return data

    def _get_cliente(self, invoice):
        sql = "SELECT rfc, nombre FROM receptores WHERE id=%s"
        self._cursor.execute(sql, [invoice['id_cliente']])
        obj = self._cursor.fetchone()
        if not obj is None:
            data = {
                'rfc': obj['rfc'],
                'slug': to_slug(obj['nombre']),
            }
            return data

        if not invoice['xml']:
            return {}

        doc = parse_xml(invoice['xml'])
        version = doc.attrib['version']
        node = doc.find('{}Receptor'.format(PRE[version]))
        rfc = node.attrib['rfc']
        nombre = node.attrib['nombre']

        tipo_persona = 1
        if rfc == 'XEXX010101000':
            tipo_persona = 4
        elif rfc == 'XAXX010101000':
            tipo_persona = 3
        elif len(rfc) == 12:
            tipo_persona = 2

        data = {
            'tipo_persona': tipo_persona,
            'rfc': rfc,
            'nombre': nombre,
            'slug': to_slug(nombre),
            'es_cliente': True,
            'es_activo': False,
        }
        if not rfc in self._clientes_rfc:
            self._clientes_rfc.append(rfc)
            self._clientes.append(data)

        data = {
            'rfc': data['rfc'],
            'slug': data['slug'],
        }
        return data

    def _get_detalles(self, id):
        sql = "SELECT * FROM cfdidetalle WHERE id_cfdi=%s"
        self._cursor.execute(sql, [id])
        rows = self._cursor.fetchall()

        fields = (
            ('categoria', 'categoria'),
            ('cantidad', 'cantidad'),
            ('unidad', 'unidad'),
            ('noidentificacion', 'clave'),
            ('descripcion', 'descripcion'),
            ('valorunitario', 'valor_unitario'),
            ('importe', 'importe'),
            ('numero', 'pedimento'),
            ('fecha', 'fecha_pedimento'),
            ('aduana', 'aduana'),
            ('cuentapredial', 'cuenta_predial'),
            ('descuento', 'descuento'),
            ('precio', 'precio_final'),
        )

        data = []
        for row in rows:
            new = {t: row[s] for s, t in fields if row[s]}
            data.append(new)

        return data

    def _get_impuestos(self, id):
        sql = "SELECT * FROM cfdiimpuestos WHERE id_cfdi=%s"
        self._cursor.execute(sql, [id])
        rows = self._cursor.fetchall()

        tasas = {
            '0': 0.0,
            '16': 0.16,
            '16.00': 0.16,
            '11': 0.11,
            '-10': 0.10,
            '-2': 0.02,
            '-0.5': 0.005,
            '-2/3': 0.106667,
            '-10.6666': 0.106667,
            '-10.666666': 0.106667,
            '-10.66660': 0.106667,
            '-4': 0.04,
        }

        data = []
        for row in rows:
            filtro = {
                'name': row['impuesto'],
                'tasa': tasas[row['tasa']],
                'tipo': row['tipo'][0],
            }
            new = {
                'importe': row['importe'],
                'filtro': filtro
            }
            data.append(new)

        return data

    def _cfdifacturas(self):
        sql = "SELECT * FROM cfdifacturas"
        self._cursor.execute(sql)
        rows = self._cursor.fetchall()
        fields = (
            ('version', 'version'),
            ('serie', 'serie'),
            ('folio', 'folio'),
            ('fecha', 'fecha'),
            ('fecha_timbrado', 'fecha_timbrado'),
            ('formadepago', 'forma_pago'),
            ('condicionesdepago', 'condiciones_pago'),
            ('subtotal', 'subtotal'),
            ('descuento', 'descuento'),
            ('tipocambio', 'tipo_cambio'),
            ('moneda', 'moneda'),
            ('total', 'total'),
            ('tipodecomprobante', 'tipo_comprobante'),
            ('metododepago', 'metodo_pago'),
            ('lugarexpedicion', 'lugar_expedicion'),
            ('totalimpuestosretenidos', 'total_retenciones'),
            ('totalimpuestostrasladados', 'total_traslados'),
            ('xml', 'xml'),
            ('id_cliente', 'cliente'),
            ('notas', 'notas'),
            ('uuid', 'uuid'),
            ('cancelada', 'cancelada'),
        )
        data = []
        totals = len(rows)
        for i, row in enumerate(rows):
            msg = '\tImportando factura {} de {}'.format(i+1, totals)
            log.info(msg)

            new = {t: row[s] for s, t in fields}

            for _, f in fields:
                new[f] = new[f] or ''


            new['fecha'] = new['fecha'].replace(microsecond=0)
            if new['fecha_timbrado']:
                new['fecha_timbrado'] = new['fecha_timbrado'].replace(microsecond=0)
            else:
                new['fecha_timbrado'] = None

            new['estatus'] = 'Timbrada'
            if new['cancelada']:
                new['estatus'] = 'Cancelada'

            if not new['uuid']:
                new['uuid'] = None
            elif new['uuid'] in('ok', '123', '??', 'X'):
                new['uuid'] = None
                new['estatus'] = 'Cancelada'
                new['cancelada'] = True

            if new['xml'] is None:
                new['xml'] = ''

            new['pagada'] = True
            new['total_mn'] = round(row['tipocambio'] * row['total'], 2)
            new['detalles'] = self._get_detalles(row['id'])
            new['impuestos'] = self._get_impuestos(row['id'])
            new['cliente'] = self._get_cliente(row)
            data.append(new)
        return data

    def _receptores(self):
        sql = "SELECT * FROM receptores"
        self._cursor.execute(sql)
        rows = self._cursor.fetchall()

        fields = (
            ('rfc', 'rfc'),
            ('nombre', 'nombre'),
            ('calle', 'calle'),
            ('noexterior', 'no_exterior'),
            ('nointerior', 'no_interior'),
            ('colonia', 'colonia'),
            ('municipio', 'municipio'),
            ('estado', 'estado'),
            ('pais', 'pais'),
            ('codigopostal', 'codigo_postal'),
            ('extranjero', 'es_extranjero'),
            ('activo', 'es_activo'),
            ('fechaalta', 'fecha_alta'),
            ('notas', 'notas'),
        )
        data = []

        sql1 = "SELECT correo FROM correos WHERE id_padre=%s"
        sql2 = "SELECT telefono FROM telefonos WHERE id_padre=%s"
        totals = len(rows)
        for i, row in enumerate(rows):
            msg = '\tImportando cliente {} de {}'.format(i+1, totals)
            log.info(msg)
            new = {t: row[s] for s, t in fields}
            new['slug'] = to_slug(new['nombre'])
            new['es_cliente'] = True
            if new['fecha_alta'] is None:
                new['fecha_alta'] = str(now())
            else:
                new['fecha_alta'] = str(new['fecha_alta'])

            for _, f in fields:
                new[f] = new[f] or ''
            if new['es_extranjero']:
                new['tipo_persona'] = 4
            elif new['rfc'] == 'XAXX010101000':
                new['tipo_persona'] = 3
            elif len(new['rfc']) == 12:
                new['tipo_persona'] = 2

            self._cursor.execute(sql1, (row['id'],))
            tmp = self._cursor.fetchall()
            if tmp:
                new['correo_facturas'] = ', '.join([r[0] for r in tmp])

            self._cursor.execute(sql2, (row['id'],))
            tmp = self._cursor.fetchall()
            if tmp:
                new['telefonos'] = ', '.join([r[0] for r in tmp])

            data.append(new)
        return data


class ImportFacturaLibre(object):

    def __init__(self, path, rfc):
        self._rfc = rfc
        self._con = None
        self._cursor = None
        self._error = ''
        self._is_connect = self._connect(path)
        self._clientes = []
        self._clientes_rfc = []

    @property
    def error(self):
        return self._error

    @property
    def is_connect(self):
        return self._is_connect

    def _validate_rfc(self):
        sql = "SELECT rfc FROM emisor LIMIT 1"
        self._cursor.execute(sql)
        obj = self._cursor.fetchone()
        if obj is None:
            self._error = 'No se encontró al emisor: {}'.format(self._rfc)
            return False

        if not DEBUG:
            if obj['rfc'] != self._rfc:
                self._error = 'Los datos no corresponden al RFC: {}'.format(self._rfc)
                return False

        return True

    def _connect(self, path):
        try:
            self._con = sqlite3.connect(path)
            self._con.row_factory = sqlite3.Row
            self._cursor = self._con.cursor()
            return self._validate_rfc()
        except Exception as e:
            log.error(e)
            self._error = 'No se pudo conectar a la base de datos'
            return False

    def close(self):
        try:
            self._cursor.close()
            self._con.close()
        except:
            pass
        return

    def import_data(self):
        data = {}
        tables = (
            ('receptores', 'Socios'),
            ('cfdfacturas', 'Facturas'),
            ('categorias', 'Categorias'),
        )
        for source, target in tables:
            data[target] = self._get_table(source)

        data['Socios'] += self._clientes

        return data

    def _get_table(self, table):
        return getattr(self, '_{}'.format(table))()

    def import_productos(self):
        sql = "SELECT * FROM productos"
        self._cursor.execute(sql)
        rows = self._cursor.fetchall()

        fields = (
            ('id_categoria', 'categoria'),
            ('noIdentificacion', 'clave'),
            ('descripcion', 'descripcion'),
            ('unidad', 'unidad'),
            ('valorUnitario', 'valor_unitario'),
            ('existencia', 'existencia'),
            ('inventario', 'inventario'),
            ('codigobarras', 'codigo_barras'),
            ('CuentaPredial', 'cuenta_predial'),
            ('precio_compra', 'ultimo_precio'),
            ('minimo', 'minimo'),
        )
        data = []

        sql = """
            SELECT nombre, tasa, tipo
            FROM impuestos, productos, productosimpuestos
            WHERE productos.id=productosimpuestos.id_producto
                AND productosimpuestos.id_impuesto=impuestos.id
                AND productos.id = ?
        """
        for row in rows:
            new = {t: row[s] for s, t in fields}
            if new['categoria'] == 0:
                new['categoria'] = None
            new['descripcion'] = ' '.join(new['descripcion'].split())
            new['clave_sat'] = DEFAULT_SAT_PRODUCTO
            self._cursor.execute(sql, (row['id'],))
            impuestos = self._cursor.fetchall()
            new['impuestos'] = tuple(impuestos)
            data.append(new)

        return data

    def _categorias(self):
        sql = "SELECT * FROM categorias ORDER BY id_padre"
        self._cursor.execute(sql)
        rows = self._cursor.fetchall()

        fields = (
            ('id', 'id'),
            ('categoria', 'categoria'),
            ('id_padre', 'padre'),
        )
        data = []

        for row in rows:
            new = {t: row[s] for s, t in fields}
            if new['padre'] == 0:
                new['padre'] = None
            data.append(new)

        return data

    def _get_cliente(self, invoice):
        sql = "SELECT rfc, nombre FROM receptores WHERE id=?"
        self._cursor.execute(sql, [invoice['id_cliente']])
        obj = self._cursor.fetchone()
        if not obj is None:
            data = {
                'rfc': obj['rfc'],
                'slug': to_slug(obj['nombre']),
            }
            return data

        if not invoice['xml']:
            return {}

        doc = parse_xml(invoice['xml'])
        version = doc.attrib['version']
        node = doc.find('{}Receptor'.format(PRE[version]))
        rfc = node.attrib['rfc']
        nombre = node.attrib['nombre']

        # ~ Validaciones especiales
        tipo_persona = 1
        if rfc == 'XEXX010101000':
            tipo_persona = 4
        elif rfc == 'XAXX010101000':
            tipo_persona = 3
        elif len(rfc) == 12:
            tipo_persona = 2

        data = {
            'tipo_persona': tipo_persona,
            'rfc': rfc,
            'nombre': nombre,
            'slug': to_slug(nombre),
            'es_cliente': True,
            'es_activo': False,
        }
        if not rfc in self._clientes_rfc:
            self._clientes_rfc.append(rfc)
            self._clientes.append(data)

        data = {
            'rfc': data['rfc'],
            'slug': data['slug'],
        }
        return data

    def _get_detalles(self, id):
        sql = "SELECT * FROM cfddetalle WHERE id_cfd=?"
        self._cursor.execute(sql, [id])
        rows = self._cursor.fetchall()

        fields = (
            ('categoria', 'categoria'),
            ('cantidad', 'cantidad'),
            ('unidad', 'unidad'),
            ('noIdentificacion', 'clave'),
            ('descripcion', 'descripcion'),
            ('valorUnitario', 'valor_unitario'),
            ('importe', 'importe'),
            ('numero', 'pedimento'),
            ('fecha', 'fecha_pedimento'),
            ('aduana', 'aduana'),
            ('CuentaPredial', 'cuenta_predial'),
            ('alumno', 'alumno'),
            ('curp', 'curp'),
            ('nivel', 'nivel'),
            ('autorizacion', 'autorizacion'),
        )

        data = []
        for row in rows:
            new = {t: row[s] for s, t in fields if row[s]}
            data.append(new)

        return data

    def _get_impuestos(self, id):
        sql = "SELECT * FROM cfdimpuestos WHERE id_cfd=?"
        self._cursor.execute(sql, [id])
        rows = self._cursor.fetchall()

        tasas = {
            '0': 0.0,
            '16': 0.16,
            '16.00': 0.16,
            '-16': 0.16,
            '11': 0.11,
            '-10': 0.10,
            '-2': 0.02,
            '-0.5': 0.005,
            '-2/3': 0.106667,
            '-10.6667': 0.106667,
            '-10.6666': 0.106667,
            '-10.666666': 0.106667,
            '-10.66660': 0.106667,
            '-10.67': 0.106667,
            '-10.66666666666667': 0.106667,
            '-4': 0.04,
            '1': 0.01,
            '25': 0.25,
            '26.5': 0.265,
            '30': 0.30,
            '8': 0.08,
        }

        data = []
        for row in rows:
            # ~ print (id, dict(row))
            filtro = {
                'name': row['nombre'],
                'tasa': tasas[row['tasa']],
                'tipo': row['tipo'][0],
            }
            new = {
                'importe': row['importe'],
                'filtro': filtro
            }
            data.append(new)

        return data

    def _cfdfacturas(self):
        sql = "SELECT * FROM cfdfacturas"
        self._cursor.execute(sql)
        rows = self._cursor.fetchall()
        fields = (
            ('version', 'version'),
            ('serie', 'serie'),
            ('folio', 'folio'),
            ('fecha', 'fecha'),
            ('fecha_timbrado', 'fecha_timbrado'),
            ('formaDePago', 'forma_pago'),
            ('condicionesDePago', 'condiciones_pago'),
            ('subTotal', 'subtotal'),
            ('descuento', 'descuento'),
            ('TipoCambio', 'tipo_cambio'),
            ('Moneda', 'moneda'),
            ('total', 'total'),
            ('tipoDeComprobante', 'tipo_comprobante'),
            ('metodoDePago', 'metodo_pago'),
            ('LugarExpedicion', 'lugar_expedicion'),
            ('totalImpuestosRetenidos', 'total_retenciones'),
            ('totalImpuestosTrasladados', 'total_traslados'),
            ('xml', 'xml'),
            ('id_cliente', 'cliente'),
            ('notas', 'notas'),
            ('uuid', 'uuid'),
            ('donativo', 'donativo'),
            ('estatus', 'estatus'),
            ('regimen', 'regimen_fiscal'),
            ('xml_acuse', 'acuse'),
        )
        data = []
        for row in rows:
            row = dict(row)
            if not 'xml_acuse'in row:
                row['xml_acuse'] = ''

            new = {t: row[s] for s, t in fields}
            if not 'uuid' in new or not new['uuid']:
                new['uuid'] = None
            if not 'xml' in new or new['xml'] is None:
                new['xml'] = ''
            if row['estatus'] == 'Pagada':
                new['pagada'] = True
            elif row['estatus'] in ('Cancelada', 'Validada'):
                new['cancelada'] = True

            if new['fecha'] is None:
                new['fecha'] = str(now())

            new['total_mn'] = round(row['TipoCambio'] * row['total'], 2)
            new['detalles'] = self._get_detalles(row['id'])
            new['impuestos'] = self._get_impuestos(row['id'])
            new['cliente'] = self._get_cliente(row)
            data.append(new)
        return data

    def _receptores(self):
        sql = "SELECT * FROM receptores"
        self._cursor.execute(sql)
        rows = self._cursor.fetchall()

        fields = (
            ('rfc', 'rfc'),
            ('nombre', 'nombre'),
            ('calle', 'calle'),
            ('noExterior', 'no_exterior'),
            ('noInterior', 'no_interior'),
            ('colonia', 'colonia'),
            ('municipio', 'municipio'),
            ('estado', 'estado'),
            ('pais', 'pais'),
            ('codigoPostal', 'codigo_postal'),
            ('extranjero', 'es_extranjero'),
            ('activo', 'es_activo'),
            ('fechaalta', 'fecha_alta'),
            ('notas', 'notas'),
            ('cuentaCliente', 'cuenta_cliente'),
            ('cuentaProveedor', 'cuenta_proveedor'),
            ('saldoCliente', 'saldo_cliente'),
            ('saldoProveedor', 'saldo_proveedor'),
            ('esCliente', 'es_cliente'),
            ('esProveedor', 'es_proveedor'),
        )
        data = []

        sql1 = "SELECT correo FROM correos WHERE id_cliente=?"
        sql2 = "SELECT telefono FROM telefonos WHERE id_cliente=?"
        for row in rows:
            new = {t: row[s] for s, t in fields}
            new['slug'] = to_slug(new['nombre'])
            new['fecha_alta'] = str(parser.parse(new['fecha_alta']))
            for _, f in fields:
                new[f] = new[f] or ''
            if new['es_extranjero']:
                new['tipo_persona'] = 4
            elif new['rfc'] == 'XAXX010101000':
                new['tipo_persona'] = 3
            elif len(new['rfc']) == 12:
                new['tipo_persona'] = 2

            self._cursor.execute(sql1, (row['id'],))
            tmp = self._cursor.fetchall()
            if tmp:
                new['correo_facturas'] = ', '.join([r[0] for r in tmp])

            self._cursor.execute(sql2, (row['id'],))
            tmp = self._cursor.fetchall()
            if tmp:
                new['telefonos'] = ', '.join([r[0] for r in tmp])

            data.append(new)
        return data


class ImportCFDI(object):

    def __init__(self, xml):
        self._doc = xml
        self._pre = ''

    def _relacionados(self):
        data = {}
        node = self._doc.find('{}CfdiRelacionados'.format(self._pre))
        if not node is None:
            data = CaseInsensitiveDict(node.attrib.copy())
        return data

    def _emisor(self):
        emisor = self._doc.find('{}Emisor'.format(self._pre))
        data = CaseInsensitiveDict(emisor.attrib.copy())
        node = emisor.find('{}RegimenFiscal'.format(self._pre))
        if not node is None:
            data['regimen_fiscal'] = node.attrib['Regimen']
        return data

    def _receptor(self):
        node = self._doc.find('{}Receptor'.format(self._pre))
        data = CaseInsensitiveDict(node.attrib.copy())
        node = node.find('{}Domicilio'.format(self._pre))
        if not node is None:
            data.update(node.attrib.copy())
        return data

    def _conceptos(self):
        data = []
        conceptos = self._doc.find('{}Conceptos'.format(self._pre))
        for c in conceptos.getchildren():
            values = CaseInsensitiveDict(c.attrib.copy())
            data.append(values)
        return data

    def _impuestos(self):
        data = {}
        node = self._doc.find('{}Impuestos'.format(self._pre))
        if not node is None:
            data = CaseInsensitiveDict(node.attrib.copy())
        return data

    def _timbre(self):
        node = self._doc.find('{}Complemento/{}TimbreFiscalDigital'.format(
            self._pre, PRE['TIMBRE']))
        data = CaseInsensitiveDict(node.attrib.copy())
        data.pop('SelloCFD', None)
        data.pop('SelloSAT', None)
        data.pop('Version', None)
        return data

    def get_data(self):
        invoice = CaseInsensitiveDict(self._doc.attrib.copy())
        invoice.pop('certificado', '')
        invoice.pop('sello', '')
        self._pre = PRE[invoice['version']]

        relacionados = self._relacionados()
        emisor = self._emisor()
        receptor = self._receptor()
        conceptos = self._conceptos()
        impuestos = self._impuestos()
        timbre = self._timbre()

        invoice.update(relacionados)
        invoice.update(emisor)
        invoice.update(receptor)
        invoice.update(impuestos)
        invoice.update(timbre)

        data = {
            'invoice': invoice,
            'emisor': emisor,
            'receptor': receptor,
            'conceptos': conceptos,
        }

        return data


def print_ticket(data, info):
    p = PrintTicket(info)
    return p.printer(data)


def import_products(rfc):
    name = '{}_products.ods'.format(rfc.lower())
    path = _join(PATH_MEDIA, 'tmp', name)
    if not is_file(path):
        return (), 'No se encontró la plantilla'

    if APP_LIBO:
        app = LIBO()
        if app.is_running:
            return app.products(path)

    return (), 'No se encontro LibreOffice'


def import_invoice(rfc):
    name = '{}_invoice.ods'.format(rfc.lower())
    path = _join(PATH_MEDIA, 'tmp', name)
    if not is_file(path):
        return (), 'No se encontró la plantilla'

    if APP_LIBO:
        app = LIBO()
        if app.is_running:
            return app.invoice(path)

    return (), 'No se encontro LibreOffice'


def calc_to_date(value):
    return datetime.date.fromordinal(int(value) + 693594)


def get_days(start, end):
    return (end - start).days + 1


def log_file(name, msg='', kill=False):
    path = _join(PATH_MEDIA, 'tmp', '{}.log'.format(name))

    if kill:
        _kill(path)
        return

    with open(path, 'a') as fh:
        line = '{} : {}\n'.format(str(now()), msg)
        fh.write(line)
    return


def get_log(name):
    data = ''
    name = '{}.log'.format(name)
    path = _join(PATH_MEDIA, 'tmp', name)
    if is_file(path):
        data = open(path).read()
    return data, name


def get_timbres(auth):
    from .pac import Finkok as PAC

    if DEBUG:
        return '-d'

    pac = PAC(auth)
    timbres = pac.client_get_timbres(auth['RFC'])
    if pac.error:
        return '-e'

    return timbres


def truncate(value):
    return trunc(value * 100) / 100


def validate_path_bk():
    path_bk = _join(str(Path.home()), DIR_FACTURAS)
    if not os.path.isdir(path_bk):
        msg = 'No existe la carpeta'
        return {'ok': False, 'msg': msg}

    return {'ok': True, 'msg': path_bk}


def respaldar_db(values, path_bk):
    user = values[0].lower()
    db = loads(values[1])['name']
    path = _join(path_bk, '{}.bk'.format(user))
    args = 'pg_dump -U postgres -Fc {} > "{}"'.format(db, path)
    _call(args)
    return


def validate_rfc(value):
    msg = ''
    if len(value) < 12:
        msg = 'Longitud inválida del RFC'
        return msg
    l = 4
    if len(value)==12:
        l = 3
    s = value[0:l]
    r = re.match('[A-ZÑ&]{%s}' % l, s)
    msg = 'Caracteres inválidos al {} del RFC'
    if not r:
        return msg.format('inicio')
    s = value[-3:]
    r = re.match('[A-Z0-9]{3}', s)
    if not r:
        return msg.format('final')
    s = value[l:l+6]
    r = re.match('[0-9]{6}', s)
    msg = 'Fecha inválida en el RFC'
    if not r:
        return msg
    try:
        datetime.datetime.strptime(s, '%y%m%d')
        return ''
    except:
        return msg


def parse_xml2(xml_str):
    return etree.fromstring(xml_str.encode('utf-8'))



